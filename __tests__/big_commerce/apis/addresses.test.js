const { setupEnv } = require('../../index.test');
describe('Big Commerce Addresses Test', () => {
    let Address;
    beforeAll(() => {
        setupEnv();
        Address = require('../../../big_commerce/apis/address');
    });
    it('Test get addresses', async () => {
        const addresses = await Address.getAddresses();
        expect(addresses.data).toBeDefined();
        expect(Array.isArray(addresses.data)).toBeTruthy();
    });
    it('Test get address', async () => {
        const addressId = 1;
        const address = await Address.getAddress(addressId);
        expect(Array.isArray(address.data)).toBeTruthy();
        expect(address.data[0].id).toBe(addressId);
    });
});
