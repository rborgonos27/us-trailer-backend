const { setupEnv } = require('../../index.test');
describe('Big Commerce Category Test', () => {
    let Category;
    beforeAll(() => {
        setupEnv();
        Category = require('../../../big_commerce/apis/category');
    });
    it('Test get categories', async () => {
        const categories = await Category.getCategories();
        expect(categories.data).toBeDefined();
        expect(Array.isArray(categories.data)).toBeTruthy();
    });
    it('Test get category', async () => {
        const categoryId = 23;
        const category = await Category.getCategory(categoryId);
        expect(category.data).toBeDefined();
        expect(category.data.id).toBe(categoryId);
    });
});
