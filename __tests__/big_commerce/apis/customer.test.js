const { setupEnv } = require('../../index.test');
describe('Big Commerce Customer Test', () => {
    let Customer;
    beforeAll(() => {
        setupEnv();
        Customer = require('../../../big_commerce/apis/customer');
    });
    it('Test get customers', async () => {
        const customers = await Customer.getCustomers();
        expect(customers.data).toBeDefined();
        expect(Array.isArray(customers.data)).toBeTruthy();
    });
    it('Test get customer', async () => {
        const customerId = 1;
        const customer = await Customer.getCustomer(customerId);
        expect(Array.isArray(customer.data)).toBeTruthy();
        expect(customer.data[0].id).toBe(customerId);
    });
    it('Test get customer groups', async () => {
        const customerGroups = await Customer.getCustomerGroups();
        expect(Array.isArray(customerGroups)).toBeTruthy();
    });
});
