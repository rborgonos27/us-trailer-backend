const { setupEnv } = require('../../index.test');
describe('Big Order Test', () => {
    let Order;
    beforeAll(() => {
        setupEnv();
        Order = require('../../../big_commerce/apis/order');
    });
    it('Test get orders', async () => {
        const orders = await Order.getAllOrders();
        expect(orders).toBeDefined();
        expect(Array.isArray(orders)).toBeTruthy();
    });
    it('Test get order', async () => {
        const orderId = 231001;
        const order = await Order.getOrders(orderId);
        expect(order).toBeDefined();
        expect(order.id).toBe(orderId);
    });
    it('Test get order shipments ', async () => {
        const orderId = 231001;
        const orderShipments = await Order.getOrderShipments(orderId);
        expect(orderShipments === '').toBeTruthy();
    });
    it('Test get order shipments address ', async () => {
        const orderId = 231001;
        const orderShipmentAddress = await Order.getOrderShipmentAddresses(orderId);
        expect(orderShipmentAddress[0].order_id).toBe(orderId);
        expect(Array.isArray(orderShipmentAddress)).toBeTruthy();
    });
    it('Test get order products ', async () => {
        const orderId = 231001;
        const orderProducts = await Order.getOrderProducts(orderId);
        expect(orderProducts[0].order_id).toBe(orderId);
        expect(Array.isArray(orderProducts)).toBeTruthy();
    });
});
