const { setupEnv } = require('../../index.test');
describe('Big Product Test', () => {
    let Product;
    beforeAll(() => {
        setupEnv();
        Product = require('../../../big_commerce/apis/product');
    });
    it('Test get products', async () => {
        const products = await Product.getProducts();
        expect(Array.isArray(products.data)).toBeTruthy();
    });
    it('Test get product', async () => {
        const productId = 77;
        const product = await Product.getProduct(productId);
        expect(product).toBeDefined();
        expect(product.data.id).toBe(productId);
    });
    it('Test get product images ', async () => {
        const productId = 77;
        const productImages = await Product.getProductImage(productId);
        expect(Array.isArray(productImages.data)).toBeTruthy();
        expect(productImages.data[0].product_id).toBe(productId);
    });
    it('Test get order shipments address ', async () => {
        const productId = 77;
        const productCustomFields = await Product.customFields.getCustomFields(productId);
        expect(Array.isArray(productCustomFields.data)).toBeTruthy();
    });
});
