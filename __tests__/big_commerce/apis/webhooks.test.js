const { setupEnv } = require('../../index.test');
describe('Big Commerce Webhooks Test', () => {
    let Webhooks;
    beforeAll(() => {
        setupEnv();
        Webhooks = require('../../../big_commerce/apis/webhooks');
    });
    it('Test get webhooks', async () => {
        const webhooks = await Webhooks.getWebhooks();
        expect(Array.isArray(webhooks)).toBeTruthy();
    });
    it('Test get webhooks crud', async () => {
        const body = {
            scope: 'store/product/updated',
            destination: 'https://665b65a6.ngrok.io/webhooks',
            is_active: true
        };
        const webhooks = await Webhooks.addWebhook(body);
        expect(webhooks).toBeDefined();
        const webHookId = webhooks.id;
        body.is_active = false;
        const updatedWebhook = await Webhooks.updateWebhooks(webHookId, body);
        expect(updatedWebhook.is_active).toBeFalsy();
        await Webhooks.deleteWebhooks(webHookId);
    });
});
