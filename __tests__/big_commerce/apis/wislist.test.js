const { setupEnv } = require('../../index.test');
describe('Big Commerce Wishlist Test', () => {
    let Wishlist;
    beforeAll(() => {
        setupEnv();
        Wishlist = require('../../../big_commerce/apis/wishlist');
    });
    it('Test get wishlist', async () => {
        const categories = await Wishlist.getWishLists();
        expect(categories.data).toBeDefined();
        expect(Array.isArray(categories.data)).toBeTruthy();
    });
});
