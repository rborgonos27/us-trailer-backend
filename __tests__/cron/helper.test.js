const { matchedHelper, otherHelper } = require('../../cron/helper');

const {
    matchBigCommerceSKU,
    getShopAllId,
    matchCategory,
    matchProductInventory,
    matchProductImages,
    matchCustomerAddress,
    matchedAddressContact,
    matchState,
    matchStateByName,
    getMatchBigCommerceToP21,
    getItemCategoryByItem,
    getParentCategoryByItem,
    getCategoryById,
    getMatchP21ToBigCommerce,
    getParentCategory,
    getCategoryByParent,
    matchedCustomerContacts,
    matchedCustomerFromBCToP21,
    matchedCustomerFromP21ToBc,
    matchedOrderFromP21ToBc,
    mapField,
} = matchedHelper;


const {
    getLocationName,
    getSumInventory,
    convertInventoryToCustomFields,
} = otherHelper;

describe('Test Cron match helper', () => {
    it('should Match Big Commerce SKU', () => {
        const bigCommerceProducts = [
            {
                sku: 'test01',
                name: 'test 01'
            },
            {
                sku: 'test02',
                name: 'test 02'
            },
            {
                sku: 'test03',
                name: 'test 03'
            }
        ];
        let skus = [
            'test01',
            'test02'
        ];
        const skuMatched = skus.map(sku => {
            const matched = matchBigCommerceSKU(bigCommerceProducts, sku);
            expect(Array.isArray(matched)).toBeTruthy();
            expect(matched.length).toBeTruthy();
            expect(matched[0].sku).toBe(sku);
            return matched;
        });
        expect(skuMatched.length > 0).toBeTruthy();
    });
    it('should Get Shop All id', () => {
        const showAllId = 1;
        const categories = [
            {
                id: 1,
                name: 'Shop All',
            },
            {
                id: 2,
                name: 'Doors',
            },
            {
                id: 3,
                name: 'Air Products',
            }
        ];
        const shopAll = getShopAllId(categories);
        expect(shopAll).toBe(showAllId);
    });
    it('should match category to big commerce', () => {
        const categories = [
            {
                id: 1,
                name: 'Trailer',
            },
            {
                id: 2,
                name: 'Doors',
            },
            {
                id: 3,
                name: 'Air Products',
            },
            {
                id: 4,
                name: 'Legs and Accessories',
            },
            {
                id: 5,
                name: 'shop supplies & Accessories',
            }
        ];
        let matchCat = matchCategory(categories, 'air products');

        expect(Array.isArray(matchCat)).toBeTruthy();
        expect(matchCat[0].id).toBe(3);

        matchCat = matchCategory(categories, 'doors');
        expect(Array.isArray(matchCat)).toBeTruthy();
        expect(matchCat[0].id).toBe(2);

        matchCat = matchCategory(categories, 'trailer');
        expect(Array.isArray(matchCat)).toBeTruthy();
        expect(matchCat[0].id).toBe(1);

        matchCat = matchCategory(categories, 'shop supplies');
        expect(Array.isArray(matchCat)).toBeTruthy();
        expect(matchCat[0].id).toBe(5);

    });
    it('should match product inventory', () => {
        const inventories = [
            {
                inv_mast_uid: 1,
                value: 100
            },
            {
                inv_mast_uid: 3,
                value: 120
            },
            {
                inv_mast_uid: 4,
                value: 300
            },
            {
                inv_mast_uid: 7,
                value: 30
            },
            {
                inv_mast_uid: 66,
                value: 44
            }
        ];
        const invUid = 3;
        const productInv = matchProductInventory(inventories, invUid);
        expect(Array.isArray(productInv)).toBeTruthy();
        expect(productInv[0].inv_mast_uid).toBe(invUid);
        expect(productInv[0].value).toBe(120);
    });
    it('should match item id to image', () => {
        const itemImages = [
            {
                PartID: 'FRUTKA9699',
                URL: 'https://test.id1'
            },
            {
                PartID: 'FRVTKA9699',
                URL: 'https://test.id2'
            },
            {
                PartID: 'FRWTKA9699',
                URL: 'https://test.id3'
            }
        ];
        let matchImage = matchProductImages(itemImages, '*FRV-TKA-9699');
        expect(Array.isArray(matchImage)).toBeTruthy();
        expect(matchImage[0].URL).toBe('https://test.id2');

        matchImage = matchProductImages(itemImages, '****FRW-TKA9-699');
        expect(Array.isArray(matchImage)).toBeTruthy();
        expect(matchImage[0].URL).toBe('https://test.id3');
    });
    it('should match address contact', () => {
       const contacts = [
           {
               address_id: '0001',
               first_name: 'test 1',
               last_name: 'contact'
           },
           {
               address_id: '0002',
               first_name: 'test 2',
               last_name: 'contact'
           },
           {
               address_id: '0003',
               first_name: 'test 3',
               last_name: 'contact'
           }
       ];
       const matchContact = matchedAddressContact(contacts, '0002');
       expect(Array.isArray(matchContact)).toBeTruthy();
       expect(matchContact[0].address_id).toBe('0002');
    });
    it('should match customer address', () => {
        const customerAddress = [
            {
                id: '001',
                address:'Test Address 1',
            },
            {
                id: '002',
                address:'Test Address 2',
            },
            {
                id: '003',
                address:'Test Address 3',
            },
            {
                id: '004',
                address:'Test Address 4',
            }
        ];
        const matchedCustomerAddress = matchCustomerAddress(customerAddress, '003');
        expect(Array.isArray(matchedCustomerAddress)).toBeTruthy();
        expect(matchedCustomerAddress[0].id).toBe('003');
    });
    it('should match state', () => {
        let state = matchState('GA');
        expect(state).toBe('Georgia');
        state = matchState('HI');
        expect(state).toBe('Hawaii');
    });
    it('should match state name', () => {
        let stateName = matchStateByName('Hawaii');
        expect(stateName).toBe('HI');
        stateName = matchStateByName('Georgia');
        expect(stateName).toBe('GA');
        stateName = matchStateByName('Michigan');
        expect(stateName).toBe('MI');
    });
    it('should match big commerce product to p21', () => {
       const bigCommerceProduct = [
           {
               id: 1,
               sku: '12345',
               name: '12345 ABCDE'
           },
           {
               id: 2,
               sku: 'ABCDE',
               name: 'ABCDE 12345'
           },
       ];
       let matchProduct = getMatchBigCommerceToP21(bigCommerceProduct, '12345');
       expect(Array.isArray(matchProduct)).toBeTruthy();
       expect(matchProduct[0].name).toBe('12345 ABCDE');
       matchProduct = getMatchBigCommerceToP21(bigCommerceProduct, 'ABCDE');
       expect(Array.isArray(matchProduct)).toBeTruthy();
       expect(matchProduct[0].name).toBe('ABCDE 12345');
    });
    it('should getItemCategoryByItem', () => {
        const itemCategories = [
            {
                invMasterUid: '123',
                itemId: '123456'
            },
            {
                invMasterUid: '456',
                itemId: '7891011'
            },
            {
                invMasterUid: '789',
                itemId: '11121314'
            }
        ];
        let itemMatched = getItemCategoryByItem(itemCategories, '123');
        expect(Array.isArray(itemMatched)).toBeTruthy();
        expect(itemMatched[0].itemId).toBe('123456');
        itemMatched = getItemCategoryByItem(itemCategories, '456');
        expect(Array.isArray(itemMatched)).toBeTruthy();
        expect(itemMatched[0].itemId).toBe('7891011');
    });
    it('should get parent category by item', () => {
        const parentCategoryItems = [
            {
                childCategoryId: 1,
                itemId: 'test01',
                category: 'category 01'
            },
            {
                childCategoryId: 2,
                itemId: 'test02',
                category: 'category 02'
            },
            {
                childCategoryId: 3,
                itemId: 'test03',
                category: 'category 03'
            }
        ];
        let parentCategoryMatch = getParentCategoryByItem(parentCategoryItems, 1);
        expect(Array.isArray(parentCategoryMatch)).toBeTruthy();
        expect(parentCategoryMatch[0].category).toBe('category 01');

        parentCategoryMatch = getParentCategoryByItem(parentCategoryItems, 3);
        expect(Array.isArray(parentCategoryMatch)).toBeTruthy();
        expect(parentCategoryMatch[0].category).toBe('category 03');
    });
    it('should get category by id', () => {
        const categories = [
            {
                id: 1,
                name: 'Doors'
            },
            {
                id: 2,
                name: 'Air Products'
            },
            {
                id: 3,
                name: 'Electrical Items'
            }
        ];
        let categoryMatch = getCategoryById(categories, 2);
        expect(Array.isArray(categoryMatch)).toBeTruthy();
        expect(categoryMatch[0].name).toBe('Air Products');
    });
    it('should get match p21 to big commerce', () => {
        const bigCommerceProducts = [
            {
                item_desc: 'Desc 1',
                item_id: 'desc01'
            },
            {
                item_desc: 'Desc 2',
                item_id: 'desc02'
            },
            {
                item_desc: 'Desc 3',
                item_id: 'desc03'
            },
            {
                item_desc: 'Desc 4',
                item_id: 'desc04'
            },
        ];
        let matchP21toBigCommerce = getMatchP21ToBigCommerce(bigCommerceProducts, 'Desc 3');
        expect(Array.isArray(matchP21toBigCommerce)).toBeTruthy();
        expect(matchP21toBigCommerce[0].item_id).toBe('desc03');
    });
    it('should get parent category', () => {
        const bigCommerceProducts = [
            {
                childCategoryId: 1,
                item_id: 'desc01'
            },
            {
                childCategoryId: 2,
                item_id: 'desc03'
            },
        ];
        let matchP21toBigCommerce = getParentCategory(bigCommerceProducts, 2);
        expect(Array.isArray(matchP21toBigCommerce)).toBeFalsy();
        expect(matchP21toBigCommerce.item_id).toBe('desc03');
    });
    it('should get category by parent', () => {
        const categories = [
            {
                id: 1,
                name: 'test 1'
            },
            {
                id: 3,
                name: 'test 2'
            },
            {
                id: 5,
                name: 'test 3'
            }
        ];
        const parentMap = getCategoryByParent(categories, {parentCategoryId: 1, name: 'test 1'});
        expect(Object.keys(parentMap).length > 0).toBeTruthy();
        expect(parentMap.id).toBe(1);
    });
    it('should get customer contacts', () => {
       const contacts = [
           {
               id: 1,
               name: 'Extream',
           },
           {
               id: 2,
               name: 'Max',
           }
       ];
       const matchedCustomerContact = matchedCustomerContacts(contacts, 'Max');
       expect(Object.keys(matchedCustomerContact).length > 0).toBeTruthy();
       expect(matchedCustomerContact[0].id).toBe(2);
    });
    it('should get customer from bc to p21', () => {
       const customers = [
           {
               id: 'max',
               company: 'Max Extream'
           },
           {
               id: 'captain',
               company: 'Captain Mathilde'
           }
       ];
       const matched = matchedCustomerFromBCToP21(customers, 'Max Extream');
        expect(Object.keys(matched).length > 0).toBeTruthy();
        expect(matched[0].id).toBe('max');
    });
    it('should get customer from 21 to bc', () => {
        const customers = [
            {
                id: 'max',
                customer_name: 'Max Extream'
            },
            {
                id: 'captain',
                customer_name: 'Captain Mathilde'
            }
        ];
        const matched = matchedCustomerFromP21ToBc(customers, 'Max Extream');
        expect(Object.keys(matched).length > 0).toBeTruthy();
        expect(matched[0].id).toBe('max');
    });
    it('should get order from p21 to bc', () => {
       const orders = [
           {
               id: 2020,
               web_reference_no: '20201'
           },
           {
               id: 2021,
               web_reference_no: '20202'
           }
       ];
       const matched = matchedOrderFromP21ToBc(orders, '20201');
       expect(Object.keys(matched).length > 0).toBeTruthy();
       expect(matched[0].id).toBe(2020);
    });
    it('should map field', () => {
       const fields = [
           {
               name: 'warehouse 1',
               id: '123'
           },
           {
               name: 'warehouse 2',
               id: '1234'
           }
       ];
       const matched = mapField(fields, 'warehouse 1');
       expect(Object.keys(matched).length > 0).toBeTruthy();
       expect(matched[0].id).toBe('123');
    });
    it('should get location name', () => {
       const locations = [
           {
               location_id: '0001',
               location_name: 'chicago'
           },
           {
               location_id: '0002',
               location_name: 'milwakee',
           }
       ];
       const locationName = getLocationName(locations, '0001');
       expect(locationName).toBe('chicago');
    });
    it('should get sum inventory', () => {
       const inventoryLocations = [
           {
               qty_on_hand: 3,
           },
           {
               qty_on_hand: 5
           }
       ];
       const sumInventory = getSumInventory(inventoryLocations);
       expect(sumInventory).toBe(8);
    });
    it('should convert inventory to custom fields', () => {
        const inventoryLocations = [
            {
                location_id: '0002',
                qty_on_hand: 3,
            },
            {
                location_id: '0001',
                qty_on_hand: 5
            }
        ];
        const locations = [
            {
                location_id: '0001',
                location_name: 'chicago'
            },
            {
                location_id: '0002',
                location_name: 'milwakee',
            }
        ];
        const converted = convertInventoryToCustomFields(inventoryLocations, locations);
        expect(converted[0].name).toBe('milwakee');
        expect(converted[1].name).toBe('chicago');
    });
});


