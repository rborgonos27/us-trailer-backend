const { setupEnv } = require('../index.test');
setupEnv();
const integration = require('../../cron/integration');
const {
    updateImportedProduct,
    getUploadMeasures,
    clearUploadMeasure,
    importCustomer,
    prepareInventoryImport,
    importInventory,
    importInventoryCustomFields,
    importProduct,
    updateProductInventoryCustomFields,
    addUpdateProduct,
    addProduct,
    addCustomer,
    updateCustomer,
    updateInventoryToBigCommerce
} = integration;
describe('Cron Integration Test', () => {
   beforeAll( () => {
       jest.setTimeout(200 * 1000);
   });
   it('should reset or clear sync measures', () => {
        clearUploadMeasure();
        const {
            successItem,
            failedItem,
            totalItem,
            errorData,
            additionalInfo
        } = getUploadMeasures();
        expect(successItem).toBe(0);
        expect(failedItem).toBe(0);
        expect(totalItem).toBe(0);
        expect(errorData.length).toBe(0);
        expect(additionalInfo.length).toBe(0);
   });
   it('should run Update Imported Product', async () => {
       await updateImportedProduct(true);
       const {
           successItem,
           failedItem,
           totalItem,
           errorData,
           additionalInfo
       } = getUploadMeasures();
       expect(successItem).toBe(0);
       expect(failedItem).toBe(0);
       expect(totalItem).toBe(0);
       expect(errorData.length).toBe(0);
       expect(additionalInfo.length).toBe(0);
   });
   it('should run import customer', async () => {
       await importCustomer(true);
       const {
           successItem,
           failedItem,
           totalItem,
           errorData,
           additionalInfo
       } = getUploadMeasures();
       expect(successItem).toBe(0);
       expect(failedItem).toBe(0);
       expect(totalItem).toBe(0);
       expect(errorData.length).toBe(0);
       expect(additionalInfo.length).toBe(0);
   });
   it('should run prepare inventory import', async () => {
      const {
          bigCommerceProducts,
          products,
          inventoryLocation,
      } = await prepareInventoryImport(true);
      expect(Array.isArray(bigCommerceProducts)).toBeTruthy();
      expect(Array.isArray(products)).toBeTruthy();
      expect(Array.isArray(inventoryLocation)).toBeTruthy();
   });
    it('should run import inventory', async () => {
        await importInventory(true);
        const {
            successItem,
            failedItem,
            totalItem,
            errorData,
            additionalInfo
        } = getUploadMeasures();
        expect(successItem).toBe(0);
        expect(failedItem).toBe(0);
        expect(totalItem).toBe(0);
        expect(errorData.length).toBe(0);
        expect(additionalInfo.length).toBe(0);
    });
    it('should run import custom fields', async () => {
        await importInventoryCustomFields(true);
        const {
            successItem,
            failedItem,
            errorData,
            additionalInfo
        } = getUploadMeasures();
        expect(successItem).toBe(0);
        expect(failedItem).toBe(0);
        expect(errorData.length).toBe(0);
        expect(additionalInfo.length).toBe(0);
    });
    it('should run import product', async () => {
        await importProduct(true);
        const {
            successItem,
            failedItem,
            totalItem,
            errorData,
            additionalInfo
        } = getUploadMeasures();
        expect(successItem).toBe(0);
        expect(failedItem).toBe(0);
        // expect(totalItem).toBe(0);
        expect(errorData.length).toBe(0);
        expect(additionalInfo.length).toBe(0);
    });
    it('it should run update product inventory custom fields', async () => {
        const productId = 0;
        const matchProductInv = [
            {
                location_id: '0002',
                qty_on_hand: 3,
            },
            {
                location_id: '0001',
                qty_on_hand: 5
            }
        ];
        const locations = [
            {
                location_id: '0001',
                location_name: 'chicago'
            },
            {
                location_id: '0002',
                location_name: 'milwakee',
            }
        ];
        let fieldValues = await updateProductInventoryCustomFields(productId,
            matchProductInv,
            locations,
            true);
        expect(fieldValues.length).toBe(0);
        // fieldValues = await updateProductInventoryCustomFields(productId,
        //     matchProductInv,
        //     locations);
        // expect(fieldValues.length).toBe(0);
    });
    it('should run add/update product', async () => {
        const params = {
            name: 'name',
            description: 'description',
            price: 33,
            weight: 3,
            height: 1,
            width: 4,
            depth: 3,
            sku: 'wwwq',
        };
        const productId = 0;
        const product = await addUpdateProduct(productId, params, true);
        expect(product).toBeNull();
    });
    it('should run add product', async () => {
        const params = {
            name: 'name',
            description: 'description',
            price: 33,
            weight: 3,
            height: 1,
            width: 4,
            depth: 3,
            sku: 'wwwq',
        };
        const product = await addProduct(params, true);
        expect(product).toBeNull();
    });
    it('should run add customer', async () => {
        const params = {
            name: 'name',
            description: 'description',
            price: 33,
            weight: 3,
            height: 1,
            width: 4,
            depth: 3,
            sku: 'wwwq',
        };
        const customer = await addCustomer(params, true);
        expect(customer).toBeNull();
    });
    it('should run update customer', async () => {
        const params = [
            {
                name: 'name',
                description: 'description',
                price: 33,
                weight: 3,
                height: 1,
                width: 4,
                depth: 3,
                sku: 'wwwq'
            }
        ];
        const customer = await updateCustomer(params, true);
        expect(customer).toBeNull();
    });
    it('should update inventory', async () => {
        // 103
        const productId = 103;
        const inventoryData = {
            inventory_level: 1
        };

        const inventoryResult = await updateInventoryToBigCommerce(productId, inventoryData);
        expect(inventoryResult.data).toBeDefined();
        expect(inventoryResult.data.inventory_level).toBe(inventoryData.inventory_level);
    });
});