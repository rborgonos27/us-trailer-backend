const p21 = require('../index.test');
p21.setupEnv();
const {
    getCategoryHierarchy,
    getProducts,
    getItemCategories,
    getAllBCommerceProducts,
    getCustomer,
    getLocations,
    getContacts,
    getAddresses,
    getItemInventory,
    getOrders,
    getContactLinks,
    getOrderItems,
} = require('../../cron/service');
const moment_tz = require('moment-timezone');
const { DATE_FORMAT } = require('../../utils/constants').P21;

describe('Cron Server', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category hierarchy', async () => {
       const hierarchies = await getCategoryHierarchy(accessToken);
       expect(Array.isArray(hierarchies)).toBeTruthy();
    });
    it('should get products', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const products = await getProducts(accessToken, modifiedAt);
        expect(Array.isArray(products)).toBeTruthy();
    });
    it('should get item categories', async () => {
        const itemCategories = await getItemCategories(accessToken, true);
        expect(Array.isArray(itemCategories)).toBeTruthy();
    });
    it('should get big commerce products', async () => {
        const bigCommerceProducts = await getAllBCommerceProducts(true);
        expect(Array.isArray(bigCommerceProducts)).toBeTruthy();
    });
    it('should get p21 customers', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const customers = await getCustomer(accessToken, modifiedAt);
        expect(Array.isArray(customers)).toBeTruthy();
    });
    it('should get inventory locations', async () => {
        const locations = await getLocations(accessToken);
        expect(Array.isArray(locations)).toBeTruthy();
    });
    it('should get p21 contacts', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const contacts = await getContacts(accessToken, modifiedAt);
        expect(Array.isArray(contacts)).toBeTruthy();
    });
    it('should get p21 address', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const addresses = await getAddresses(accessToken, modifiedAt);
        expect(Array.isArray(addresses)).toBeTruthy();
    });
    it('should get item inventory', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const itemInventories = await getItemInventory(accessToken, modifiedAt);
        expect(Array.isArray(itemInventories)).toBeTruthy();
    });
    it('should get orders', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const orders = await getOrders(accessToken, modifiedAt);
        expect(Array.isArray(orders)).toBeTruthy();
    });
    it('should get contact links', async () => {
        const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
        const contactLinks = await getContactLinks(accessToken, modifiedAt);
        expect(Array.isArray(contactLinks)).toBeTruthy();
    });
    it('should get order items', async () => {
       const items = [
           {
               item_id: 'CLC',
               item_desc: '[Sample] Canvas Laundry Cart'
           },
           {
               item_id: 'TWB',
               item_desc: '[Sample] Tiered Wire Basket'
           },
           {
               item_id: 'CC3C',
               item_desc: '[Sample] Chemex Coffeemaker 4 Cup'
           }
        ];
       const orderId = 231001;
       const orderItems = await getOrderItems(orderId, items, []);
       expect(Array.isArray(orderItems)).toBeTruthy();
       expect(orderItems.length).toBe(0);
    });
});
