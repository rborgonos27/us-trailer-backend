const p21 = require('../index.test');
p21.setupEnv();
const {
    syncCustomerDataToP21,
    syncOrderDataToP21,
    syncP21OrderToBigCommerce,
} = require('../../cron/sync');

describe('Cron Sync Test', () => {
    it('should run sync customer data to p21', async () => {
        const bigCommerceToP21 = await syncCustomerDataToP21(true);
        expect(bigCommerceToP21).toBeUndefined();
    });
    it('should run sync order data to p21', async () => {
        const bigCommerceToP21 = await syncOrderDataToP21(true);
        expect(bigCommerceToP21).toBeUndefined();
    });
    it('should run sync p21 data to big commerce', async () => {
        const p21ToBigCommerce = await syncP21OrderToBigCommerce(true);
        expect(p21ToBigCommerce).toBeUndefined();
    });
});