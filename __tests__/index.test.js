const Auth = require('../p21/apis/auth');
const { decrypt } = require('../utils/encryption');

describe('initial tests', () => {
  it('should run this fake tests', () => {
    expect(true).toBe(true);
  });
});

const credentials = {
  ENCRYPTED_HOST: 'e7b1a80b0f27925ec0b056cb3c47682832cdc5f534bb6868887df8572263580c',
  ENCRYPTED_PORT: /*'e9b6bc599364c93b3a7dc97bb32ec410',*/ '5ec767d6a05936ebf80a0e05c93a71f5',
  ENCRYPTED_USERNAME: '26cade2af40ae4993f667fa0b14408f4',
  ENCRYPTED_PASSWORD: '53649e9ab4b979d8cf84c2198305f109',
};

const setupEnv = () => {
  jest.setTimeout(120 * 1000);
  const {
    ENCRYPTED_HOST,
    ENCRYPTED_PORT,
    ENCRYPTED_USERNAME,
    ENCRYPTED_PASSWORD
  } = credentials;
  process.env.P21_HOST=decrypt(ENCRYPTED_HOST);
  process.env.P21_PORT=decrypt(ENCRYPTED_PORT);
  process.env.P21_USERNAME=decrypt(ENCRYPTED_USERNAME);
  process.env.P21_PASSWORD=decrypt(ENCRYPTED_PASSWORD);
  process.env.P21_TIMEZONE='America/Chicago';
  process.env.BC_ACCESS_TOKEN='717s3z9zmblodfzuoz2i8znz68i87bx';
  process.env.BC_CLIENT_ID='m5iozc4x22mruejkxyjh9juzdki5v9i';
  process.env.BC_CLIENT_SECRET='25269efe0b02fce83af035e0da6f23e411f5d349e2e2fc060e945e43626c223e';
  process.env.BC_NAME='ERP Integration';
  process.env.BC_STORE_HASH='8rmmcmgcxd';
  process.env.BC_VERSION='v3';
  process.env.BC_API_PATH='https://api.bigcommerce.com/stores';
  process.env.MAX_API_RETRIES=3;
};

const beforeAll = async () => {
  setupEnv();
  const authResponse = await Auth(process.env.P21_USERNAME, process.env.P21_PASSWORD);
  const accessToken = authResponse.AccessToken;
  expect(accessToken).toBeDefined();
  return accessToken;
};

module.exports = {
  setupEnv,
  beforeAll,
  credentials,
};
