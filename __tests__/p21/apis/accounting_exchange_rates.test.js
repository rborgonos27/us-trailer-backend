const p21 = require('../../index.test');
const AccountingExchangeRates = require('../../../p21/apis/accounting_exchange_rates');

describe('Accounting Exchange Rates API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get Accounting Exchange Rates records', async () => {
        const customerResponse = await AccountingExchangeRates(accessToken);
        expect(Array.isArray(customerResponse)).toBeTruthy();
    });
});
