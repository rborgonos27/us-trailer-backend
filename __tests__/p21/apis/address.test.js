const p21 = require('../../index.test');
const Address = require('../../../p21/apis/address');

describe('Address API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    // it('should get customer records', async () => {
    //     const customerResponse = await Customer.getCustomer(accessToken);
    //     expect( Array.isArray(customerResponse)).toBeTruthy();
    // });
    it('should get specific address', async () => {
        const addressId = '100009';
        const addressResponse = await Address.getAddress(accessToken, addressId);
        expect(Object.keys(addressResponse).length > 0).toBeTruthy();
        expect(addressResponse.AddressId).toBe(Number(addressId));
    });
});
