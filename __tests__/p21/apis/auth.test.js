const p21 = require('../../index.test');
const Auth = require('../../../p21/apis/auth');

describe('Auth API', () => {
    beforeAll(() => {
        p21.setupEnv();
    });
    it('should authenticate and return access token', async () => {
        const authResponse = await Auth(process.env.P21_USERNAME, process.env.P21_PASSWORD);
        expect(authResponse.AccessToken).toBeDefined();
    });
});
