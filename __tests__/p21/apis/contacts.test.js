const p21 = require('../../index.test');
const Contacts = require('../../../p21/apis/contacts');

describe('Contacts API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    // it('should get contacts records', async () => {
    //     const contactResponse = await Contacts(accessToken);
    //     expect(Array.isArray(contactResponse)).toBeTruthy();
    // });
    it('should get specific contact', async () => {
        const contactId = '5737';
        const contactResponse = await Contacts.getContact(accessToken, contactId);
        expect(Object.keys(contactResponse).length > 0).toBeTruthy();
    });
});
