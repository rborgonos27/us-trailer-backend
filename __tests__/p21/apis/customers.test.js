const p21 = require('../../index.test');
const Customer = require('../../../p21/apis/customer');

describe('Customers API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    // it('should get customer records', async () => {
    //     const customerResponse = await Customer.getCustomer(accessToken);
    //     expect( Array.isArray(customerResponse)).toBeTruthy();
    // });
    it('should get specific customer', async () => {
        const companyCustomer = {
            companyId: 'USTP',
            customerId: '100034'
        };
        const customerResponse = await Customer.getCustomer(accessToken, companyCustomer);
        expect(Object.keys(customerResponse).length > 0).toBeTruthy();
        expect(customerResponse.CustomerId).toBe(Number(companyCustomer.customerId));
        expect(customerResponse.CompanyId).toBe(companyCustomer.companyId);
    });
    it('should add customer', async () => {
        // const companyCustomer = {
        //     CompanyId:"USTP",
        //     CustomerName:"Four13 Group4",
        //     SalesrepId:"5961",
        //     CodRequiredFlag:"N",
        //     PricingMethod:"Manual",
        //     WebEnabledFlag:"Y"
        // };
        // const customerResponse = await Customer.saveCustomer(accessToken, companyCustomer);
        // console.log(customerResponse);
    });
});
