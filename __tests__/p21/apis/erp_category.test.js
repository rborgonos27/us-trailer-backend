const p21 = require('../../index.test');
const Category = require('../../../p21/apis/view_category');

describe('Category Data API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category records', async () => {
        const categoryResponse = await Category(accessToken);
        expect(Array.isArray(categoryResponse)).toBeTruthy();
        expect(categoryResponse[0].id).toBeDefined();
        expect(categoryResponse[0].name).toBeDefined();
        expect(categoryResponse[0].description).toBeDefined();
    });
});
