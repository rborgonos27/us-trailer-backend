const p21 = require('../../index.test');
const CategoryHierarchy = require('../../../p21/apis/view_category_hierarchy');
const { OPERATOR } = require('../../../p21/enums');

describe('Category Hierarchy Data API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category hierarchy item records', async () => {
        const filters = [
            {
                type: OPERATOR.NOT_EQUAL,
                field: 'parent_item_category_uid',
                value: 1
            }
        ];
        const categoryItemResponse = await CategoryHierarchy(accessToken, filters);
        expect(Array.isArray(categoryItemResponse)).toBeTruthy();
        expect(categoryItemResponse[0].parentCategoryId).toBeDefined();
        expect(categoryItemResponse[0].childCategoryId).toBeDefined();
    });
});
