const p21 = require('../../index.test');
const CategoryItem = require('../../../p21/apis/view_category_item');
const { OPERATOR } = require('../../../p21/enums');

describe('Category Item Data API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category item records', async () => {
        const filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'item_category_uid',
                value: 70
            }
        ];
        const categoryItemResponse = await CategoryItem(accessToken, filters);
        expect(Array.isArray(categoryItemResponse)).toBeTruthy();
        expect(categoryItemResponse[0].itemCategoryUid).toBeDefined();
        expect(categoryItemResponse[0].invMasterUid).toBeDefined();
    });
});
