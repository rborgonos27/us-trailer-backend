const p21 = require('../../index.test');
const CategoryContactLinks = require('../../../p21/apis/view_contacts_links');

describe('Category Data API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category records', async () => {
        const categoryResponse = await CategoryContactLinks(accessToken, [], 10);
        expect(Array.isArray(categoryResponse)).toBeTruthy();
        expect(categoryResponse[0].id).toBeDefined();
        expect(categoryResponse[0].name).toBeDefined();
    });
});
