const p21 = require('../../index.test');
const InvMaster = require('../../../p21/apis/view_inv_master');
const { OPERATOR } = require('../../../p21/enums');

describe('Category Item Data API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get category item records', async () => {
        const filters = [
            {
                type: OPERATOR.GREATER_THAN_EQUAL,
                field: 'inv_mast_uid',
                value: 100
            },
            {
                type: OPERATOR.NOT_EQUAL,
                field: 'delete_flag',
                value: 'Y'
            },
        ];
        const invMasterResponse = await InvMaster(accessToken, filters, 100);
        expect(Array.isArray( invMasterResponse )).toBeTruthy();
    });
});
