const p21 = require('../../index.test');
const OrderDetail = require('../../../p21/apis/view_order_detail');
const { OPERATOR } = require('../../../p21/enums');

describe('Order Detail Data API', () => {
   let accessToken = null;
   beforeAll(async () => {
      accessToken = await p21.beforeAll();
   });
   it('should get picklist data', async () => {
      const filters = [
          {
              type: OPERATOR.GREATER_THAN_EQUAL,
              field: 'oe_line_uid',
              value: 100
          },
          {
              type: OPERATOR.EQUAL,
              field: 'order_no',
              value: '1000141'
          }
      ];
      const oeLine = await OrderDetail(accessToken, filters, 100);
      expect(Array.isArray(oeLine)).toBeTruthy();
      expect(oeLine[0].line_no).toBeDefined();
      expect(oeLine[0].oe_line_uid).toBeDefined();
      expect(oeLine[0].inv_mast_uid).toBeDefined();
   });
});