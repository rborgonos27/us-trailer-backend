const p21 = require('../../index.test');
const PickList = require('../../../p21/apis/view_picklist');
const { OPERATOR } = require('../../../p21/enums');

describe('Picklist Data API', () => {
   let accessToken = null;
   beforeAll(async () => {
      accessToken = await p21.beforeAll();
   });
   it('should get picklist data', async () => {
      const filters = [
          {
              type: OPERATOR.GREATER_THAN_EQUAL,
              field: 'pick_ticket_no',
              value: 100
          },
          {
            type: OPERATOR.NOT_EQUAL,
            field: 'delete_flag',
            value: 'Y'
          }
      ];
      const picklist = await PickList(accessToken, filters, 100);
       expect(Array.isArray(picklist)).toBeTruthy();
       expect(picklist[0].company_id).toBeDefined();
       expect(picklist[0].order_no).toBeDefined();
   });
});