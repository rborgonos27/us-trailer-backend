const p21 = require('../../index.test');
const PickListDetail = require('../../../p21/apis/view_picklist_details');
const { OPERATOR } = require('../../../p21/enums');

describe('Picklist Data API', () => {
   let accessToken = null;
   beforeAll(async () => {
      accessToken = await p21.beforeAll();
   });
   it('should get picklist data', async () => {
      const filters = [
          {
              type: OPERATOR.GREATER_THAN_EQUAL,
              field: 'pick_ticket_no',
              value: 100
          }
      ];
      const picklist = await PickListDetail(accessToken, filters, 100);
      expect(Array.isArray(picklist)).toBeTruthy();
      expect(picklist[0].pick_ticket_no).toBeDefined();
      expect(picklist[0].oe_line_no).toBeDefined();
   });
});