const p21 = require('../../index.test');
const Part = require('../../../p21/apis/parts');
const { OPERATOR } = require('../../../p21/enums');

describe('Part API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    it('should get part records by filter', async () => {
        const filters = [
            {
                type: OPERATOR.CONTAINS,
                field: 'ItemDesc',
                value: 'Trailer'
            }
        ];
        const partResponse = await Part.getParts(accessToken, null, filters);
        expect(Array.isArray(partResponse)).toBeTruthy();
    });
    it('should get specific part', async () => {
        const attribute = {
            itemId: '*BEN-802893'
        };
        const partResponse = await Part.getParts(accessToken, attribute);
        expect(Object.keys(partResponse).length > 0).toBeTruthy();
    });
    it('should get part records by filter & extended properties', async () => {
        const filters = [
            {
                type: OPERATOR.CONTAINS,
                field: 'ItemDesc',
                value: 'Trailer'
            },
            {
                type: OPERATOR.EXTENDED_PROPERTIES,
                field: '',
                value: 'Locations'
            }
        ];
        const partResponse = await Part.getParts(accessToken, null, filters);
        expect(Array.isArray(partResponse)).toBeTruthy();
        expect(partResponse[0].Locations).toBeDefined();
    });
});

