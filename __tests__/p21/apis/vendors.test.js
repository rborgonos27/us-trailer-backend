const p21 = require('../../index.test');
const Vendors = require('../../../p21/apis/vendors');

describe('Vendors API', () => {
    let accessToken = null;
    beforeAll(async () => {
        accessToken = await p21.beforeAll();
    });
    // it('should get vendor records', async () => {
    //     const vendorResponse = await Vendors(accessToken);
    //     expect( Array.isArray(vendorResponse)).toBeTruthy();
    // });
    it('should get specific vendor', async () => {
        const companyVendor = {
            companyId: 'USTP',
            vendorId: '100009'
        };
        const vendorResponse = await Vendors(accessToken, companyVendor);
        expect(Object.keys(vendorResponse).length > 0).toBeTruthy();
        expect(vendorResponse.VendorId).toBe(Number(companyVendor.vendorId));
        expect(vendorResponse.CompanyId).toBe(companyVendor.companyId);
    });
});
