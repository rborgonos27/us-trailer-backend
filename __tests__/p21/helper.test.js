const {
    requestService,
    options,
    getQueryString,
    writeOption,
    writeRequestService
} = require('../../p21/helper');
const { decrypt } = require('../../utils/encryption');
const { API, OPERATOR } = require('../../p21/enums');
const { credentials } = require('../index.test');

describe('P21 Helper', () => {
   it('should construct options config', () => {
       process.env.P21_HOST='host.test.com';
       process.env.P21_PORT=1123;
       const header = {
           un: 'test',
           pw: 'test'
       };
       const config = options('/test/path', header);
       expect(config.host).toBe(process.env.P21_HOST);
       expect(config.port).toBe(process.env.P21_PORT);
       expect(config.method).toBe('GET');
       expect(config.headers.un).toBeDefined();
       expect(config.headers.pw).toBeDefined();
   });
    it('it should create option with REST DEBUG', () => {
        const path = '/get_test';
        const header = {
            'Content-Type': 'application/json'
        };
        process.env.REST_DEBUG = 1;
        const config = options(path, header);
        // console.log(config);
        expect(Object.keys(config).length > 0).toBeTruthy();
        expect(config.host).toBeDefined();
        expect(config.port).toBeDefined();
        expect(config.path).toBe(path);
    });
   it('should get response using requestService', async () => {
       const {
           ENCRYPTED_HOST,
           ENCRYPTED_PORT,
           ENCRYPTED_USERNAME,
           ENCRYPTED_PASSWORD
       } = credentials;
       process.env.P21_HOST=decrypt(ENCRYPTED_HOST);
       process.env.P21_PORT=decrypt(ENCRYPTED_PORT);
       process.env.P21_USERNAME=decrypt(ENCRYPTED_USERNAME);
       process.env.P21_PASSWORD=decrypt(ENCRYPTED_PASSWORD);
       const headers = {
         username: process.env.P21_USERNAME,
         password: process.env.P21_PASSWORD,
       };
       const config = options(`${API.AUTH}?format=json`, headers, 'POST');
       const authResponse = await requestService(config);
       expect(authResponse.AccessToken).toBeDefined();
   });
   it('should construct url query string', () => {
       const filters = [
           {
               type: OPERATOR.CONTAINS,
               field: 'ItemDesc',
               value: 'trailer',
           }
       ];
       let queryString = getQueryString(filters);
       let expectedQueryString = encodeURI('?$query=substringof(\'trailer\', ItemDesc)');
       expect(queryString).toBe(expectedQueryString);
       filters.push(
           {
               type: OPERATOR.GREATER_THAN,
               field: 'Price1',
               value: '0',
           }
       );
       queryString = getQueryString(filters);
       expectedQueryString = encodeURI('?$query=substringof(\'trailer\', ItemDesc) and Price1 gt \'0\'');
       expect(queryString).toBe(expectedQueryString);
       queryString = getQueryString(filters, 'or');
       expectedQueryString = encodeURI('?$query=substringof(\'trailer\', ItemDesc) or Price1 gt \'0\'');
       expect(queryString).toBe(expectedQueryString);
       filters.push(
           {
               type: OPERATOR.EXTENDED_PROPERTIES,
               field: '',
               value: '*',
           }
       );
       queryString = getQueryString(filters, 'or');
       expectedQueryString = encodeURI('?extendedproperties=*&$query=substringof(\'trailer\', ItemDesc) or Price1 gt \'0\'');
       expect(queryString).toBe(expectedQueryString);
   });
   it('should write request service', async () => {
        const config =  { host: 'jsonplaceholder.typicode.com',
            port: 443,
            path: '/posts',
            method: 'POST',
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            body: {
                title: 'foo',
                body: 'bar',
                userId: 1
            }
        };
        const response = await writeRequestService(config);
        expect(response).toBeDefined();
   });
   it('should create write option', () => {
        const path = '/test';
        const header = {
            'Content-Type': 'application/json'
        };
        const body = {
            name: 'mine',
            email: 'mine@email.net'
        };
        const method = 'POST';
        const config = writeOption(path, header, method, body);
        // console.log(config);
        expect(Object.keys(config).length > 0).toBeTruthy();
        expect(config.host).toBeDefined();
        expect(config.port).toBeDefined();
        expect(config.path).toBe(path);
        expect(config.method).toBe(method);
        process.env.P21_HOST = 'host.prod.com';
        process.env.P21_PORT = 1123;
        process.env.TEST_P21_HOST = 'host.test.com';
        process.env.TEST_P21_PORT = 1124;
        process.env.TEST_WRITE_P21 = 1;
        const anotherConfig = writeOption(path, header);
        expect(anotherConfig.host).toBe(process.env.TEST_P21_HOST);
        expect(anotherConfig.port).toBe(process.env.TEST_P21_PORT);
        expect(anotherConfig.path).toBe(path);
        expect(anotherConfig.method).toBe(method);
   });
   it('it should create write option with REST DEBUG', () => {
       const path = '/test';
       const header = {
           'Content-Type': 'application/json'
       };
       const body = {
           name: 'mine',
           email: 'mine@email.net'
       };
       process.env.REST_DEBUG = 1;
       const method = 'POST';
       const config = writeOption(path, header, method, body);
       // console.log(config);
       expect(Object.keys(config).length > 0).toBeTruthy();
       expect(config.host).toBeDefined();
       expect(config.port).toBeDefined();
       expect(config.path).toBe(path);
       expect(config.method).toBe(method);
   });
});
