const { setupEnv } = require('../index.test');
setupEnv();

const {
    validateDetails,
    // deleteCustomer,
    // updateCustomer,
    // updateCustomerAddress,
    addCustomer,
} = require('../../service/customer');

describe('Service Customer Test', () => {
    it('it should run validate details', async () => {
        const id = 2;
        const {
            customer,
            address,
            p21Customer }  = await validateDetails(id);
        expect(customer).toBeDefined();
        expect(address).toBeDefined();
        expect(Array.isArray(p21Customer)).toBeDefined();
    });
    it('it should add customer', async () => {
        const id = 2;
        const customer = await addCustomer(id, true);
        expect(customer).toBeUndefined();
    });
});