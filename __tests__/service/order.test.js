const { setupEnv } = require('../index.test');
setupEnv();

const {
    validateDetails,
    createOrderParameter,
} = require('../../service/order');

describe('Service Order Test', () => {
    it('it should run validate details', async () => {
        const id = 231001;
        const { p21, bigCommerce }  = await validateDetails(id);
        expect(p21).toBeDefined();
        expect(bigCommerce).toBeDefined();
    });
    it('it should run create order parameter', async () => {
        const order = {
            id: 10000,
            date_created: 'Wed, 22 Jul 2020 21:58:49 +0000',
            base_shipping_cost: 15
        };
        const orderItems = [
            {
                sku: 'TSI-003',
                quantity: 3,
                base_price: 20
            }
        ];
        const wishlist = [
            {
                "id": 28,
                "customer_id": 4,
                "name": "Job List 1",
                "is_public": false,
                "token": "5a729a9f-3c47-4c4a-9ca0-78f4f7952974",
                "items": [
                    {
                        "id": 23,
                        "product_id": 10828
                    },
                    {
                        "id": 26,
                        "product_id": 11674
                    }
                ]
            },
        ];

        const bigCommerce = {
          order,
          orderItems,
          wishlist
        };

        const p21Customer = {
            company_id: 'USTP',
            customer_id: 3
        };
        const p21Contact = {
            id: 33
        };
        const p21Item = [{
            item_id: 'TSI-003',
            default_selling_unit: 'ROLL',
        }];

        const p21 = { p21Customer, p21Contact, p21Item };

        const orderParam = createOrderParameter(bigCommerce, p21);
        expect(orderParam).toBeDefined();
        expect(Array.isArray(orderParam.Lines.list)).toBeTruthy();
    });
});