const columns = require('../../utils/columns');
const design = {
    label: 'User',
    cols: {
        'Email': {type: String},
        'FirstName': {type: String},
        'LastName': {type: String},
        'Password': {type: String},
        'Role': {type: String},
        'DateCreated': { type: Date },
        'DateUpdated': { type: Date }
    }
};

describe('Utils GenerateSchema columns helper', () => {
    it('Test generate schema for mongoose', () => {
        const schema = columns.GenerateSchema(design.cols);
        const expected = {
            Email: String,
            FirstName: String,
            LastName: String,
            Password: String,
            Role: String,
            DateCreated: Date,
            DateUpdated: Date,
        };
        expect(schema).toStrictEqual(expected);
    });
    it('Test generate schema for mongoose with value suffix', () => {
        const schema = columns.GenerateSchema(design.cols, true);
        const expected = {
            Email: String,
            Email_val: String,
            FirstName: String,
            FirstName_val: String,
            LastName: String,
            LastName_val: String,
            Password: String,
            Password_val: String,
            Role: String,
            Role_val: String,
            DateCreated: Date,
            DateCreated_val: Date,
            DateUpdated: Date,
            DateUpdated_val: Date
        };
        expect(schema).toStrictEqual(expected);
    });
});