const encryption = require('../../utils/encryption');

describe('Encryption test run', () => {
    it('should encrypt and decrypt text', () => {
        const textToEncrypt = 'my text';
        const encryptedText = encryption.encrypt(textToEncrypt);
        const decryptedText = encryption.decrypt(encryptedText);
        expect(decryptedText).toBe(textToEncrypt);
    });
});
