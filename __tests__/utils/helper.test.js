const {
    extractHostname,
    buildQueryString,
    getDataFromArray,
    checkArray,
    checkInvalidOrEmptyArray,
    checkInputString,
    checkValidArray,
    restLog,
    restLogTime,
    escapeQuote,
} = require('../../utils/helper');

describe('Utils Helper tests', () => {
   it('should get the proper domain', () => {
       const url = 'https://domain.test.com?filter=134&for=any';
       const host = extractHostname(url);
       expect(host).toBe('domain.test.com');
   });
   it('should get the proper domain with none https url', () => {
       const url = 'http://domain.test.com?filter=134&for=any';
       const host = extractHostname(url);
       expect(host).toBe('domain.test.com');
   });
   it('should get the proper domain with none https test url', () => {
       const url = 'http://domain.test.com/test/path?filter=134&for=any';
       const host = extractHostname(url);
       expect(host).toBe('domain.test.com');
   });
   it('should get domain name', () => {
       const url = 'domain.test.com/test';
       const host = extractHostname(url);
       expect(host).toBe('domain.test.com');
   });
   it('should check array', () => {
       const data = [
           { id: 1, name: 'test '},
           { id: 2, name: 'examine' }
       ];
       const checkData = checkArray(data);
       expect(checkData).toBeTruthy();
   });
   it('should check invalid or empty array', () => {
       const data = [];
       const checkEmptyArray = checkInvalidOrEmptyArray(data);
       expect(checkEmptyArray).toBeTruthy();
       const empty = {};
       const checkEmptyData = checkInvalidOrEmptyArray(empty);
       expect(checkEmptyData).toBeTruthy();
   });
   it('should check input string', () => {
      const data = '';
      const validData = checkInputString(data);
      expect(validData).toBeFalsy();
   });
   it('should check valid array', () => {
       const data = [ 1, 2, 3 ];
       const validData = checkValidArray(data);
       expect(validData).toBeTruthy();
   });
});

describe('Utils Helper buildQueryString', () => {
   it('should get query parameter', () => {
       const filters = [
           {
               field: 'name',
               value: 'this value'
           }
       ];
       const queryString = buildQueryString(filters);
       expect(queryString).toBe('name=this value');
   });
   it('should get query parameters from set of filters', () => {
       const filters = [
           {
               field: 'name',
               value: 'this value'
           },
           {
               field: 'is_visible',
               value: true
           }
       ];
       const queryString = buildQueryString(filters);
       expect(queryString).toBe('name=this value&is_visible=true');
   });
   it ('should not build query string', () => {
      const filter = [];
      const queryString = buildQueryString(filter);
      expect(queryString === '').toBeTruthy();
   });
});

describe('Utils Helper getDataFromArray', () => {
   it('should get data from array', () => {
       const data = [
           { name: 'test', value: 'test value' }
       ];
       const valueData = getDataFromArray(data);
       expect(Array.isArray(valueData)).toBeFalsy();
       expect(Object.keys(valueData).length > 0).toBeTruthy();
   });
   it('should not get data from array', () => {
       const data = [];
       const valueData = getDataFromArray(data);
       expect(Array.isArray(valueData)).toBeFalsy();
       expect(valueData).toBeNull();
   });
});

describe('Utils Rest Log Tests', () => {
    it('should get rest log', () => {
       process.env.REST_DEBUG = '1';
       let isLog = restLog('test label', 'test value');
       expect(isLog).toBeTruthy();
       process.env.REST_DEBUG = '0';
       isLog = restLog('test label', 'test value');
       expect(isLog).toBeFalsy();
    });
    it('should get rest time log', () => {
        process.env.REST_DEBUG = '1';
        let isLog = restLogTime('test label');
        expect(isLog).toBeTruthy();
        isLog = restLogTime('test label', true);
        expect(isLog).toBeTruthy();
        process.env.REST_DEBUG = '0';
        isLog = restLogTime('test label');
        expect(isLog).toBeFalsy();
        isLog = restLogTime('test label', true);
        expect(isLog).toBeFalsy();
    });
});

describe('Utils Escape Quote', () => {
    it('should add escape quote', () => {
        let inputString = `Miller's Truck`;
        inputString = escapeQuote(inputString);
        expect(inputString).toBe(`Miller''s Truck`);
    });
});
