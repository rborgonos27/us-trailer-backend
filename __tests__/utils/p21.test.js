const { getToken } = require('../../utils/p21');
const { decrypt } = require('../../utils/encryption');
const { credentials } = require('../index.test');


describe('Utils P21 tests', () => {
    it('should get access token', async () => {
        const {
            ENCRYPTED_HOST,
            ENCRYPTED_PORT,
            ENCRYPTED_USERNAME,
            ENCRYPTED_PASSWORD
        } = credentials;
        process.env.P21_HOST=decrypt(ENCRYPTED_HOST);
        process.env.P21_PORT=decrypt(ENCRYPTED_PORT);
        process.env.P21_USERNAME=decrypt(ENCRYPTED_USERNAME);
        process.env.P21_PASSWORD=decrypt(ENCRYPTED_PASSWORD);
        const token = await getToken();
        expect(token).toBeDefined();
        expect(token.length > 10).toBeTruthy();
    });
});
