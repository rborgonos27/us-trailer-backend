const { setupEnv } = require('../index.test');
setupEnv();
const { hookScope } = require('../../webhooks/enums');
const { hookCondition } = require('../../webhooks/customer');

const { CREATE_CUSTOMER, UPDATE_CUSTOMER, UPDATE_CUSTOMER_ADDRESS, DELETE_CUSTOMER } = hookScope;

describe('Webhook Customer Test', () => {
    it('should run create customer', async () => {
        const process = await hookCondition(1, CREATE_CUSTOMER, true);
        expect(process).toBeUndefined();
    });
    it('should run update customer', async () => {
        const process = await hookCondition(1, UPDATE_CUSTOMER, true);
        expect(process).toBeUndefined();
    });
    it('should run update customer address', async () => {
        const process = await hookCondition(1, UPDATE_CUSTOMER_ADDRESS, true);
        expect(process).toBeUndefined();
    });
    it('should run delete customer address', async () => {
        const process = await hookCondition(1, DELETE_CUSTOMER, true);
        expect(process).toBeUndefined();
    });
});
