const { setupEnv } = require('../index.test');
setupEnv();
const { initialize, deleteExistingWebHooks, deleteWebHook, addWebHook } = require('../../webhooks/index');

describe('Webhooks tests', () => {
    beforeAll(() => {
       jest.setTimeout(120 * 1000);
    });
    it('should run delete hook', async () => {
        const hookResult = await deleteWebHook(1,true);
        expect(hookResult).toBeNull();
    });
    it('should run add web hooks', async () => {
        const hookResult = await addWebHook({}, true);
        expect(hookResult).toBeNull();
    });
    it('should run initialize', async () => {
        const hookInitialize = await initialize(true);
        expect(hookInitialize).toBeUndefined();
    });
    it('should run delete existing web hooks', async () => {
        const deleteExistingWebhooks = await deleteExistingWebHooks(true);
        expect(deleteExistingWebhooks).toBeUndefined();
    });
});