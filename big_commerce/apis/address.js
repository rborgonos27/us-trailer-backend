const bigCommerce = require('../helper');
const { buildQueryString } = require('../../utils/helper');
const { API } = require('../enum');

/***
 *
 * @param queryString
 * @returns {Promise<*>}
 */
const getAddresses = (queryString = '') => {
    const query = queryString !== '' ? `?${queryString}` : '';
    bigCommerce.apiVersion = 'v3';
    const endPoint = `${API.ADDRESSES}${query}`;
    return bigCommerce.get(endPoint);
};

/***
 *
 * @param customer
 * @returns {Promise<*>}
 */
const addAddress = (customer) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(API.ADDRESSES, customer);
};

/***
 *
 * @param id
 * @param customer
 * @returns {Promise<*>}
 */
const updateAddress = (id, customer) => {
    return bigCommerce.put(`${API.ADDRESSES}/${id}`, customer);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const deleteAddress = (id) => {
    return bigCommerce.delete(`${API.ADDRESSES}?id:in=${id}`);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const getAddress = (id) => {
    return bigCommerce.get(`${API.ADDRESSES}?id:in=${id}`);
};

module.exports = {
    getAddresses,
    getAddress,
    addAddress,
    updateAddress,
    deleteAddress,
};