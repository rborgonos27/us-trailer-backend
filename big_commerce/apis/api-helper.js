require('dotenv').config();
const axios = require('axios');
const FormData = require('form-data');

const encodeForm = (data) => {
    return Object.keys(data)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
        .join('&');
};

const executeRequest = (path, method = 'GET', data = null) => {
    return new Promise(function (resolve, reject) {
        const protocol = 'https';
        const host = `store-${process.env.BC_STORE_HASH}.mybigcommerce.com`;
        const url = `${protocol}://${host}/${path}`;
        const options = {
            method,
            headers: getHeaders(),
            url
        };
        if (data) {
            options.data = encodeForm(data)
        }
        console.log('options', options);
        axios(options).then(data => {
            resolve(data);
        }).catch(error => {
            console.log('Error', error.response.data);
            reject(error.response.data);
        });
    });
};

/**
 *
 * @param params
 * @returns {string}
 */
const getQueryString = (params) => {
    const parameters = Object.keys(keys);
    if (parameters.length === 0) {
        return '';
    }
    let query = [];
    parameters.map(key => {
        console.log(params[key]);
        query.push(`${key}=${params[key]}`);
    });
    return query.join('&');
};

/***
 *
 * @returns {{Accept: string, "Content-Type": string}}
 */
const getHeaders = () => {
    return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'origin': '*',
        'referer': '',
        'X-Requested-With': 'XMLHttpRequest',
        'Access-Control-Allow-Origin': '*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive'
    };
};

module.exports = {
    getHeaders,
    executeRequest,
    getQueryString,
};
