const bigCommerce = require('../helper');
const { API } = require('../enum');

/**
 *
 * @returns {Promise<*>}
 */
const getBrands = (query) => {
    return bigCommerce.get(`${API.BRANDS}${query}`);
};

/***
 *
 * @param brand
 * @returns {Promise<*>}
 */
const addBrand = (brand) => {
    return bigCommerce.post(API.BRANDS, brand);
};

/***
 *
 * @param id
 * @param brand
 * @returns {Promise<*>}
 */
const updateBrand = (id, brand) => {
    return bigCommerce.put(`${API.BRANDS}/${id}`, brand);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const deleteBrand = (id) => {
    return bigCommerce.delete(`${API.BRANDS}/${id}`);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const getBrand = (id) => {
    return bigCommerce.get(`${API.BRANDS}/${id}`);
};

/***
 *
 * @param id
 * @param imageData
 * @returns {Promise<*>}
 */
const addBrandImage = (id, imageData) => {
    return bigCommerce.post(`${API.BRANDS}/${id}`, imageData);
};

module.exports = {
    getBrands,
    addBrand,
    updateBrand,
    deleteBrand,
    getBrand,
    addBrandImage,
};
