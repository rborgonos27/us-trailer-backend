const bigCommerce = require('../helper');
const { buildQueryString } = require('../../utils/helper');
const { API } = require('../enum');

const getCategories = (filters=[]) => {
    bigCommerce.apiVersion = 'v3';
    const queryString = filters.length > 0 ? '?' + buildQueryString(filters) : '';
    return bigCommerce.get(`${API.CATEGORIES}${queryString}`);
};

const addCategory = (category) => {
    return bigCommerce.post(API.CATEGORIES, category);
};

const updateCategory = (id, category) => {
    return bigCommerce.put(`${API.CATEGORIES}/${id}`, category);
};

const deleteCategory = (id) => {
    return bigCommerce.delete(`${API.CATEGORIES}/${id}`);
};

const getCategory = (id) => {
    return bigCommerce.get(`${API.CATEGORIES}/${id}`);
};

module.exports = {
    getCategories,
    getCategory,
    addCategory,
    updateCategory,
    deleteCategory,
};
