const bigCommerce = require('../helper');
const { API } = require('../enum');

/***
 *
 * @param queryString
 * @returns {Promise<*>}
 */
const getCustomers = (queryString = '') => {
    const query = queryString !== '' ? `?${queryString}` : '';
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.CUSTOMERS}${query}`);
};

/***
 *
 * @param customer
 * @returns {Promise<*>}
 */
const addCustomer = (customer) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(API.CUSTOMERS, customer);
};

/***
 *
 * @param id
 * @param customer
 * @returns {Promise<*>}
 */
const updateCustomer = (customer) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.put(`${API.CUSTOMERS}`, customer);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const deleteCustomer = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.delete(`${API.CUSTOMERS}/${id}`);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const getCustomer = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.CUSTOMERS}?id:in=${id}`);
};

/**
 *
 * @param queryString
 * @returns {Promise<*>}
 */
const getCustomerGroups = (queryString = '') => {
    const query = queryString !== '' ? `?${queryString}` : '';
    bigCommerce.apiVersion = 'v2';
    return bigCommerce.get(`${API.CUSTOMER_GROUPS}${query}`);
};

module.exports = {
    getCustomers,
    getCustomer,
    addCustomer,
    updateCustomer,
    deleteCustomer,
    getCustomerGroups,
};