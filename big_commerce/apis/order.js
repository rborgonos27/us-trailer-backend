const bigCommerce = require('../helper');
const { API } = require('../enum');

const getAllOrders = (queryString = '') => {
    bigCommerce.apiVersion = 'v2';
    const query = queryString !== '' ? `?${queryString}` : '';
    const endPoint = `${API.ORDERS}${query}`;
    return bigCommerce.get(endPoint);
};

const getOrders = (orderId = '', queryString = '') => {
    bigCommerce.apiVersion = 'v2';
    const query = queryString !== '' ? `?${queryString}` : '';
    const endPoint = `${API.ORDERS}/${orderId}${query}`;
    return bigCommerce.get(endPoint);
};

const getOrderShipments = (orderId) => {
    bigCommerce.apiVersion = 'v2';
    const endPoint = `${API.ORDERS}/${orderId}/shipments`;
    return bigCommerce.get(endPoint);
};

const getShippingAddresses = (orderId) => {
    bigCommerce.apiVersion = 'v2';
    const endPoint = `${API.ORDERS}/${orderId}/shipping_addresses`;
    return bigCommerce.get(endPoint);
};

const getOrderShipmentAddresses = (orderId, addressId = '') => {
    bigCommerce.apiVersion = 'v2';
    const shippingAddressId = addressId === '' ? '' : `/${addressId}`;
    const endPoint = `${API.ORDERS}/${orderId}/shipping_addresses${shippingAddressId}`;
    return bigCommerce.get(endPoint);
};

const getOrderProducts = (orderId, productId = '') => {
    bigCommerce.apiVersion = 'v2';
    const orderProductId = productId === '' ? '' : `/${productId}`;
    const endPoint = `${API.ORDERS}/${orderId}/products${orderProductId}`;
    return bigCommerce.get(endPoint);
};

const updateOrder = (orderId, body) => {
    bigCommerce.apiVersion = 'v2';
    const endPoint = `${API.ORDERS}/${orderId}`;
    return bigCommerce.put(endPoint, body);
};

const createOrderShipment = (orderId, body) => {
    bigCommerce.apiVersion = 'v2';
    const endPoint = `${API.ORDERS}/${orderId}/shipments`;
    return bigCommerce.post(endPoint, body);
};

const updateOrderShipment = (orderId, shipmentId, body) => {
    bigCommerce.apiVersion = 'v2';
    const endPoint = `${API.ORDERS}/${orderId}/shipments/${shipmentId}`;
    return bigCommerce.post(endPoint, body);
};

module.exports = {
    getOrders,
    getAllOrders,
    getOrderShipments,
    getOrderShipmentAddresses,
    getOrderProducts,
    updateOrder,
    createOrderShipment,
    updateOrderShipment,
    getShippingAddresses,
};
