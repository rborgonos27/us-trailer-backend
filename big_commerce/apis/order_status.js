const bigCommerce = require('../helper');
const { buildQueryString } = require('../../utils/helper');
const { API } = require('../enum');

const getOrdersStatuses = (filters=[]) => {
    bigCommerce.apiVersion = 'v3';
    const queryString = filters.length > 0 ? '?' + buildQueryString(filters) : '';
    return bigCommerce.get(`${API.ORDER_STATUS}${queryString}`);
};

const getOrderStatus = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.ORDER_STATUS}/${id}`);
};

module.exports = {
    getOrdersStatuses,
    getOrderStatus,
};
