const bigCommerce = require('../helper');
const { API } = require('../enum');

const getPriceLists = (queryString = '') => {
    bigCommerce.apiVersion = 'v3';
    const query = queryString !== '' ? `?${queryString}` : '';
    return bigCommerce.get(`${API.PRICELISTS}${query}`);
};

const addPriceList = (body) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(`${API.PRICELISTS}`, body);
};

const getPriceList = (priceListId) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.PRICELISTS}/${priceListId}`);
};

const deletePriceList = (priceListId) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.delete(`${API.PRICELISTS}/${priceListId}`);
};

const updatePriceList = (body) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.put(`${API.PRICELISTS}`, body);
};

const addPriceListAssignment = (body) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(`${API.PRICELISTS_ASSIGNMENTS}`, body);
};

const getPriceListAssignments = () => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.PRICELISTS_ASSIGNMENTS}`);
};

const deletePriceListAssignment = (queryString = '') => {
    bigCommerce.apiVersion = 'v3';
    const query = queryString !== '' ? `?${queryString}` : '';
    return bigCommerce.delete(`${API.PRICELISTS_ASSIGNMENTS}${query}`);
};

const upSertPriceListRecord = (priceListId, body) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(`${API.PRICELISTS}/${priceListId}/records`, body);
};

const getPriceListRecords = (priceListId) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.PRICELISTS}/${priceListId}/records`);
};

const deletePriceListRecords = (priceListId, queryString = '') => {
    bigCommerce.apiVersion = 'v3';
    const query = queryString !== '' ? `?${queryString}` : '';
    return bigCommerce.delete(`${API.PRICELISTS}/${priceListId}/records${query}`);
};

module.exports = {
    getPriceLists,
    addPriceList,
    getPriceList,
    deletePriceList,
    updatePriceList,
    addPriceListAssignment,
    getPriceListAssignments,
    upSertPriceListRecord,
    deletePriceListAssignment,
    getPriceListRecords,
    deletePriceListRecords,
};
