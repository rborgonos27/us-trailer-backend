const bigCommerce = require('../helper');
const { API } = require('../enum');

/***
 *
 * @returns {Promise<*>}
 */
const getProducts = (queryString = '') => {
    const query = queryString !== '' ? `?${queryString}` : '';
    const url = `${API.PRODUCTS}${query}`;
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(url);
};

/***
 *
 * @param product
 * @returns {Promise<*>}
 */
const addProduct = (product) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(API.PRODUCTS, product);
};

/***
 *
 * @param id
 * @param product
 * @returns {Promise<*>}
 */
const  updateProduct = (id, product) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.put(`${API.PRODUCTS}/${id}`, product);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const deleteProduct = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.delete(`${API.PRODUCTS}/${id}`);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const getProduct = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.PRODUCTS}/${id}`);
};

/***
 *
 * @param id
 * @param imageData
 * @returns {Promise<*>}
 */
// {
//     "is_thumbnail": true,
//     "sort_order": 1,
//     "description": "Top View",
//     "image_url": "http://www.myauroraplus.com/myaurora/parts/BLT-CBEL0420.gif"
// }
const addProductImage = (id, imageData) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.post(`${API.PRODUCTS}/${id}/images`, imageData);
};

const getProductImage = (id) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.get(`${API.PRODUCTS}/${id}/images`);
};

const removeProductImage = (productId, imageId) => {
    bigCommerce.apiVersion = 'v3';
    return bigCommerce.delete(`${API.PRODUCTS}/${productId}/images/${imageId}`);
};

const addCustomField = (productId, body) => {
    bigCommerce.apiVersion = 'v3';
    const endPoint = `${API.PRODUCTS}/${productId}/custom-fields`;
    return bigCommerce.post(endPoint, body);
};

const updateCustomField = (productId, fieldId, body)  => {
    bigCommerce.apiVersion = 'v3';
    const endPoint = `${API.PRODUCTS}/${productId}/custom-fields/${fieldId}`;
    return bigCommerce.put(endPoint, body);
};

const getCustomFields = (productId, queryString = '') => {
    bigCommerce.apiVersion = 'v3';
    const query = queryString !== '' ? `?${queryString}` : '';
    const endPoint = `${API.PRODUCTS}/${productId}/custom-fields${query}`;
    return bigCommerce.get(endPoint);
};

const removeField = (productId, fieldId) => {
    bigCommerce.apiVersion = 'v3';
    const endPoint = `${API.PRODUCTS}/${productId}/custom-fields/${fieldId}`;
    return bigCommerce.delete(endPoint);
};

const customFields = {
    addCustomField,
    updateCustomField,
    getCustomFields,
    removeField,
};

module.exports = {
    getProducts,
    addProduct,
    updateProduct,
    deleteProduct,
    getProduct,
    addProductImage,
    getProductImage,
    removeProductImage,
    customFields,
};
