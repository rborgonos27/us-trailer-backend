const apiHelper = require('./api-helper');

const forgotPassword = (email, guestToken = '') => {
    const data = {
        email
    };
    const queryString = guestToken !== '' ? `&guestTkn=${guestToken}` : '';
    const path = `login.php?action=send_password_email${queryString}`;
    return apiHelper.executeRequest(path, 'POST', data);
};

module.exports = {
  forgotPassword
};
