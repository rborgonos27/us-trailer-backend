const bigCommerce = require('../helper');
const { API } = require('../enum');

// set to version 2 only for webhooks

/***
 *
 * @returns {Promise<*>}
 */
const addWebhook = (webhook) => {
    bigCommerce.apiVersion = 'v2';
    return bigCommerce.post(API.WEBHOOKS, webhook);
};

/***
 *
 * @returns {Promise<*>}
 */
const getWebhooks = () => {
    bigCommerce.apiVersion = 'v2';
    return bigCommerce.get(API.WEBHOOKS);
};

/***
 *
 * @param id
 * @param webhook
 * @returns {Promise<*>}
 */
const updateWebhooks = (id, webhook) => {
    bigCommerce.apiVersion = 'v2';
    return bigCommerce.put(`${API.WEBHOOKS}/${id}`, webhook);
};

/***
 *
 * @param id
 * @returns {Promise<*>}
 */
const deleteWebhooks = (id) => {
    bigCommerce.apiVersion = 'v2';
    return bigCommerce.delete(`${API.WEBHOOKS}/${id}`);
};

module.exports = {
    addWebhook,
    getWebhooks,
    updateWebhooks,
    deleteWebhooks,
};
