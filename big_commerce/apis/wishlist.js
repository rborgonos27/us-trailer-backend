const bigCommerce = require('../helper');
const { API } = require('../enum');

const getWishLists = (queryString = '') => {
    bigCommerce.apiVersion = 'v3';
    const query = queryString !== '' ? `?${queryString}` : '';
    const endpoint = `${API.WISHLISTS}${query}`;
    return bigCommerce.get(endpoint);
};

module.exports = {
    getWishLists,
};
