/***
 *
 * @type {{PRODUCTS: string, CATEGORIES: string, BRANDS: string}}
 */
const API = {
  CATEGORIES: '/catalog/categories',
  BRANDS: '/catalog/brands',
  PRODUCTS: '/catalog/products',
  CUSTOMERS: '/customers',
  CUSTOMER_GROUPS: '/customer_groups',
  ADDRESSES: '/customers/addresses',
  WEBHOOKS: '/hooks',
  WISHLISTS: '/wishlists',
  PRICELISTS: '/pricelists',
  PRICELISTS_ASSIGNMENTS: '/pricelists/assignments',
  ORDERS: '/orders',
  ORDER_STATUS: '/order_statuses',
};

/***
 *
 * @type {{PHYSICAL: string, DIGITAL: string}}
 */
const PRODUCT_TYPE = {
  PHYSICAL: 'physical',
  DIGITAL: 'digital',
};

module.exports = {
  API,
  PRODUCT_TYPE,
};