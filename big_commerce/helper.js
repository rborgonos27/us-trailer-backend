const BigCommerce = require('node-bigcommerce');

const bigCommerce = new BigCommerce({
    clientId: process.env.BC_CLIENT_ID,
    accessToken: process.env.BC_ACCESS_TOKEN,
    storeHash: process.env.BC_STORE_HASH,
    responseType: 'json',
    apiVersion: 'v3',
});

module.exports = bigCommerce;
