const Category = require('./apis/category');
const Brand = require('./apis/brand');
const Product = require('./apis/product');
const Customer = require('./apis/customer');
const Order = require('./apis/order');
const PriceList = require('./apis/pricelists');
const WebHook = require('./apis/webhooks');
const WishList = require('./apis/wishlist');
const OrderStatus = require('./apis/order_status');

module.exports = {
    Category,
    Brand,
    Product,
    Customer,
    Order,
    PriceList,
    WebHook,
    WishList,
    OrderStatus
};
