const Config = require('./models/config');
const { SYNC } = require('./cron/enum');

const configEntry = [
    {
        SyncName: SYNC.SYNC_NEW_P21_PRODUCT,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_UPDATE_BIG_COMMERCE_PRODUCT,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_EXISTING_PRODUCT_TO_BIG_COMMERCE,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_CATEGORIES,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_CATEGORY_PRODUCT,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_CATEGORY_HIERARCHY,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    },
    {
        SyncName: SYNC.SYNC_PRODUCT,
        CanRun: true,
        CreatedAt: Date(),
        UpdatedAt: Date(),
        DeletedAt: null,
    }
];

configEntry.map(async (entry) => {
    const savedEntry = await Config.model.findOne({ SyncName: entry.SyncName });
    if (!savedEntry) {
        await Config.model(entry).save();
    }
});
