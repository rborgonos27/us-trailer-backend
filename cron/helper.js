const _ = require('lodash');
const ViewCategoryItem = require('../p21/apis/view_category_item');
const { OPERATOR } = require('../p21/enums');

const {
    SHOP_ALL,
    CATEGORY_TRAILER,
    CATEGORY_TRAILER_LABEL,
    CATEGORY_DOORS,
    CATEGORY_SHOP_SUPPLIES,
    P21_SEPARATOR,
    BC_SEPARATOR
} = require('../utils/constants').BigCommerce;

const matchBigCommerceSKU = (bigCommerceProducts, sku) => {
    return _.filter(bigCommerceProducts, (product) => {
        return product.sku === sku;
    });
};

const getShopAllId = (categories) => {
    const shopAll = _.filter(categories, (cat) => {
        return cat.name === SHOP_ALL;
    });
    return Object.keys(shopAll[0]).length > 0 ? shopAll[0].id : 0;
};

const matchCategory = (bigCommerceCategories, categoryText) => {
    return  _.filter(bigCommerceCategories, function(cat) {
        const category = cat.name.toLowerCase();
        let p21Category = categoryText.toLowerCase();
        p21Category = p21Category.replace(P21_SEPARATOR, BC_SEPARATOR);
        if (category === p21Category) {
            return true;
        } else if (category === CATEGORY_TRAILER_LABEL) {
            if (p21Category.startsWith(CATEGORY_TRAILER)) {
                return true;
            }
        } else if (category === CATEGORY_DOORS) {
            if (p21Category.search(CATEGORY_DOORS) > 0) {
                return true;
            }
        } else if (p21Category === CATEGORY_SHOP_SUPPLIES) {
            if (category.startsWith(p21Category)) {
                return true;
            }
        } else {
            return false;
        }
    });
};

const matchProductInventory = (inventories, invMasterUid) => {
    return _.filter(inventories, function(inventory) {
        return inventory.inv_mast_uid === invMasterUid;
    });
};

const matchProductImages = (imageItems, productId) => {
    return _.filter(imageItems, function (item) {
        return productId.replace(/[-*]/g, '') === item.PartID.replace(/[-*]/g, '');
    });
};

const matchCustomerAddress = (addresses, customerId) => {
    return _.filter(addresses, (address) => {
        return address.id === customerId;
    })
};

const matchedAddressContact = (contacts, addressId) => {
    return _.filter(contacts, (contact) => {
        return contact.address_id === addressId;
    });
};

const matchedCustomerContacts = (contacts, customerName) => {
    return _.filter(contacts, (contact) => {
        return contact.name === customerName;
    });
};

const matchedCustomerFromBCToP21 = (customers, customerName) => {
  return _.filter(customers, (customer) => {
     return customer.company === customerName;
  });
};

const matchedCustomerFromP21ToBc = (customers, customerName) => {
    return _.filter(customers, (customer) => {
        return customer.customer_name === customerName;
    });
};

const matchedOrderFromP21ToBc = (orders, orderRef) => {
    return _.filter(orders, (order) => {
        return order.web_reference_no === `${orderRef}`;
    });
};

const matchState = (code) => {
    const states = require('./state');
    return states[code];
};

const matchStateByName = (name) => {
    const states = require('./state');
    return Object.keys(states).find(key => states[key] === name);
};

const getMatchBigCommerceToP21 = (bigCommerceProducts, item_id) => {
    return _.filter(bigCommerceProducts, function(bcProduct) {
        return bcProduct.sku === item_id;
    });
};

const getItemCategoryByItem = (itemCategories, itemId) => {
    return  _.filter(itemCategories, function(cat) {
        return cat.invMasterUid === itemId;
    });
};

const getParentCategoryByItem = (itemHierarchy, categoryId) => {
    return _.filter(itemHierarchy, function(itemOrg) {
        return itemOrg.childCategoryId == categoryId;
    });
};

const getCategoryById = (categories, parentCategoryId) => {
    return _.filter(categories, function(cat) {
        return cat.id == parentCategoryId;
    });
};

const getMatchP21ToBigCommerce = (p21Products, name) => {
    return _.filter(p21Products, function(p21product) {
        return p21product.item_desc === name;
    });
};

const getParentCategory = (categoryHierarchy, categoryId) => {
    const parentCategory = _.filter(categoryHierarchy, function (hierarchy) {
        return hierarchy.childCategoryId === categoryId;
    });
    return parentCategory.length > 0 ? parentCategory[0] : null;
};

const getCategoryByParent = (categories, parentCategory) => {
    try {
        const parentCategoryParent = _.filter(categories, function (hierarchy) {
            return hierarchy.id === parentCategory.parentCategoryId;
        });
        return parentCategoryParent.length > 0 ? parentCategoryParent[0]: null;
    } catch (error) {
        console.log(error, parentCategory);
    }

};

const getCategoryItems = async (token, category) => {
    const filters = [
        {
            type: OPERATOR.EQUAL,
            field: 'item_category_uid',
            value: Number(category.id)
        }
    ];
    return await ViewCategoryItem(token, filters);
};

const mapField = (params, fieldName) => {
    return _.filter(params, param => {
        return param.name === fieldName;
    });
};

const getSumInventory = (inventoryLocations) => {
    let totalQuantityOnHand = 0;
    inventoryLocations.map(inventory => {
        totalQuantityOnHand = Number(totalQuantityOnHand) + Number(inventory.qty_on_hand);
    });
    return totalQuantityOnHand;
};
const getLocationName = (locations, locationId) => {
    const locationName = _.filter(locations, location => {
        return location.location_id === locationId;
    });
    return Array.isArray(locationName) && locationName.length > 0 ? locationName[0].location_name : '';
};
const convertInventoryToCustomFields = (inventoryLocations, locations) => {
    return inventoryLocations.map(invLocation => {
        const quantity = Number(invLocation.qty_on_hand);
        return {
            name: `${getLocationName(locations, invLocation.location_id)}`,
            value: `${quantity}`,
        };
    });
};

const matchedHelper = {
    matchBigCommerceSKU,
    getShopAllId,
    matchCategory,
    matchProductInventory,
    matchProductImages,
    matchCustomerAddress,
    matchedAddressContact,
    matchState,
    getMatchBigCommerceToP21,
    getItemCategoryByItem,
    getParentCategoryByItem,
    getCategoryById,
    getMatchP21ToBigCommerce,
    getParentCategory,
    getCategoryByParent,
    matchedCustomerContacts,
    matchStateByName,
    matchedCustomerFromBCToP21,
    matchedCustomerFromP21ToBc,
    matchedOrderFromP21ToBc,
    mapField,
};

const bigCommerceAPI = {
  getCategoryItems,
};

const otherHelper = {
    getLocationName,
    getSumInventory,
    convertInventoryToCustomFields,
};

module.exports = {
    matchedHelper,
    bigCommerceAPI,
    otherHelper,
};
