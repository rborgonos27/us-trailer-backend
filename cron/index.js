const cron = require('node-cron');
const { updateImportedProduct, importProduct, importInventory, importInventoryCustomFields } = require('./integration');
const { syncCustomerDataToP21, syncOrderDataToP21, syncP21OrderToBigCommerce } = require('./sync');

if (process.env.RUN_CRON === '1') {
    console.log('run cron');
    cron.schedule('20 */2 * * *', () => {
        console.log('run every two hours -- sync P21 Product to Big Commerce updated products');
        updateImportedProduct();
    });

    cron.schedule('0 7 * * *', () => {
        console.log('run every 7 am utc -- sync Import P21 Product to Big Commerce');
        importProduct();
    });

    cron.schedule('0 8 * * *', () => {
        console.log('run every 8 am utc -- sync Import P21 Product Custom Fields');
        importInventoryCustomFields();
    });

    cron.schedule('10 */2 * * *', () => {
        console.log('run every 2 hours  -- sync Item Inventory to Big Commerce');
        importInventory();
    });

    cron.schedule('0 6 * * *', () => {
        console.log('run every 7 am utc -- sync Import P21 Customer to Big Commerce');
        importCustomer();
    });

    // order sync
    cron.schedule('10 6 * * *', () => {
        console.log('run every 7 am utc -- sync Big Commerce order to P21');
        syncOrderDataToP21();
    });

    // order to big commerce
    cron.schedule('*/20 * * * *', () => {
        console.log('run every 20 mins sync p21 order to Big Commerce');
        syncP21OrderToBigCommerce();
    });

    // customer sync
    cron.schedule('20 6 * * *', () => {
        console.log('run every 7 am utc -- sync Import Big Commerce to P21 Customer');
        syncCustomerDataToP21();
    });
}

/***
 * Orders will sync from BC to Prophet21 in real time (via a webhook) or near real-time.
 * Inventory sync will run at least hourly from P21 into BC.
 * Product / Item sync from P21 to BC can probably run daily in an overnight batch
 * Customer sync from P21 to BC will probably run nightly as well
 * Customer sync from BC to P21 will be real-time along with orders
 ***/
