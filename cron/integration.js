const Product = require('../big_commerce/apis/product');
const Customer = require('../big_commerce/apis/customer');
// const PriceList = require('../big_commerce/apis/pricelists');
const { getToken } = require('../utils/p21');
const moment_tz = require('moment-timezone');
const {
    matchBigCommerceSKU,
    getShopAllId,
    matchCategory,
    matchProductInventory,
    // matchProductImages,
    matchCustomerAddress,
    // matchedAddressContact,
    matchedCustomerContacts,
    matchState,
    getMatchBigCommerceToP21,
    getCategoryById,
    getItemCategoryByItem,
    getParentCategoryByItem,
    matchedCustomerFromBCToP21,
    mapField,
} = require('./helper').matchedHelper;

const {
    getSumInventory,
    convertInventoryToCustomFields,
} = require('./helper').otherHelper;

const { restLogTime, restLog } = require('../utils/helper');

const { ViewCategoryItem } = require('../p21/apis');
const {
    getCategoryHierarchy,
    getItemCategories,
    getProducts,
    getAllBCommerceProducts,
    getCustomer,
    getAddresses,
    // getContacts,
    getItemInventory,
    getContactLinks,
    getBigCommerceCustomer,
    getBigCommerceCategories,
    getCategory,
    getRecentInventoryLocation,
    getLocations,
} = require('./service');

const { DATE_FORMAT } = require('../utils/constants').P21;

const { sleep } = require('../utils/helper');

const { OPERATOR } = require('../p21/enums');
const {
    PRODUCT_AVAILABILITY,
    PRODUCT_TYPE,
    ADDRESS_TYPE,
    DEFAULT_COUNTRY_CODE,
} = require('../utils/constants').BigCommerce;

const PRICE_PERCENT_MARKUP = 1.40; // add 40%
const REMOVE_PREFIX = '**';
const MAX_RETRIES = process.env.MAX_API_RETRIES;

let successItem = 0;
let failedItem = 0;
let totalItem = 0;
let errorData = [];
let additionalInfo = [];

// for reporting purposes
let bcProductPrice = [];
let errorReport = [];

const clearUploadMeasure = () => {
    successItem = 0;
    failedItem = 0;
    totalItem = 0;
    errorData = [];
    additionalInfo = [];
};

const getUploadMeasures = () => {
  return {
     successItem,
     failedItem,
     totalItem,
     errorData,
     additionalInfo
  }
};

/************************************************
 * P21 Notes
 * -- first call p21_view_item_category_x_inv_mast
 * https://p21m.ustrailerparts.com:3443/data/erp/views/v1/p21_view_item_category_x_inv_mast?$format=json&$select=item_category_uid, inv_mast_uid
 * -- second call  p21_view_item_category_hierarchy
 * -- https://p21m.ustrailerparts.com:3443/data/erp/views/v1/p21_view_item_category_hierarchy?$format=json&$select=parent_item_category_uid, child_item_category_uid
 * -- p21_view_item_category
 * https://p21m.ustrailerparts.com:3443/data/erp/views/v1/p21_view_item_category?$format=json&$select=item_category_uid,item_category_id,item_category_desc
 * inv_mast_uid,item_id,item_desc,delete_flag,date_last_modified,extended_desc,weight,width,height,price1
 * https://p21m.ustrailerparts.com:3443/data/erp/views/v1/p21_view_inv_mast?$format=json&$top=1000&$select=inv_mast_uid,item_id,item_desc,delete_flag,date_last_modified,extended_desc,weight,width,height,price1&$filter=price1 gt 0 and delete_flag ne 'Y'
 ************************************************/

/**
 *
 * @param skipServerUpdate
 * @returns {Promise<void>}
 */
const updateImportedProduct = async (skipServerUpdate = false) => {
    restLogTime('updateImportedProduct');
    clearUploadMeasure();
    const token = await getToken();
    const bigCommerceCategories = await getBigCommerceCategories();
    restLog('**********************************');
    restLog('bigCommerceCategories: ', bigCommerceCategories.length);
    const itemHierarchy = await getCategoryHierarchy(token);
    restLog('**********************************');
    restLog('itemHierarchy: ', itemHierarchy.length);
    const categories = await getCategory(token);
    restLog('**********************************');
    restLog('categories: ', categories.length);
    const bigCommerceProducts = await getAllBCommerceProducts(skipServerUpdate);
    restLog('big commerce products', bigCommerceProducts.length);
    restLog('**********************************');
    let shopAllId = getShopAllId(bigCommerceCategories);
    const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
        .subtract(2, 'hours').format(DATE_FORMAT);
    const products = await getProducts(token, modifiedAt);
    restLog('products', products.length);
    // get modified product in the last 2 hours
    const promises = [];
    products.map(async product => {
       const productSKU = product.sku || '';
       const productName = product.name || '';
       const productDescription = product.description || '';

       if (productSKU.indexOf(REMOVE_PREFIX) === -1 &&
           productName.indexOf(REMOVE_PREFIX) === -1 &&
           productDescription.indexOf(REMOVE_PREFIX) === -1) {
           const productSku = matchBigCommerceSKU(bigCommerceProducts, product.item_id);
           if (productSku.length > 0) {
               const bcProduct = productSku[0];
               const productParam = {
                   name: product.item_desc,
                   description: !product.extended_desc ? product.item_desc : product.extended_desc,
                   price: product.price4 > 0 ? product.price4 : Number(product.price1) * PRICE_PERCENT_MARKUP,
                   weight: product.weight || 0,
                   height: product.height || 0,
                   width: product.width || 0,
                   depth: product.length || 0,
                   sku: product.item_id,
               };
               if (!skipServerUpdate) {
                   promises.push(addUpdateProduct(bcProduct.id, productParam));
               }
           } else {
               const catFilter = {
                   type: OPERATOR.EQUAL,
                   field: 'inv_mast_uid',
                   value: product.inv_mast_uid,
               };
               const catItem = await ViewCategoryItem(token, catFilter);
               const parentCategory = getParentCategoryByItem(itemHierarchy, catItem[0].itemCategoryUid);
               const category = getCategoryById(categories, parentCategory[0].parentCategoryId);
               const matched = matchCategory(bigCommerceCategories, category[0].description);
               const bcCategoriesParam = [shopAllId];
               if (matched.length > 0) {
                   bcCategoriesParam.push(matched[0].id);
               }
               const productData = {
                   name: product.item_desc,
                   type: PRODUCT_TYPE,
                   description: !product.extended_desc ? product.item_desc : product.extended_desc,
                   price: product.price4 > 0 ? product.price4 : Number(product.price1) * PRICE_PERCENT_MARKUP,
                   categories: bcCategoriesParam,
                   availability: PRODUCT_AVAILABILITY,
                   weight: product.weight || 0,
                   height: product.height  || 0,
                   width: product.width  || 0,
                   depth: product.length  || 0,
                   sku: product.item_id,
               };
               if (!skipServerUpdate) {
                   promises.push(addProduct(productData));
               }
           }
       }
    });
    fullFillPromises(promises, 'updateImportedProduct');
};

/**
 *
 * @param skipServerUpdate
 * @returns {Promise<void>}
 */
const importCustomer = async (skipServerUpdate = false) => {
    restLogTime('importCustomer');
    const bcCustomer = await getBigCommerceCustomer('', skipServerUpdate);
    restLog('bcCustomer', bcCustomer.length);
    const token = await getToken();
    let modifiedAt = '';
    if (skipServerUpdate) {
        modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
    }
    const customers = await getCustomer(token, modifiedAt);
    restLog('*******************************');
    restLog(customers.length, 'customers');
    restLog('*******************************');
    const contacts = await getContactLinks(token, modifiedAt);
    restLog(contacts.length, 'contacts');
    restLog('*******************************');
    const addresses = await getAddresses(token, modifiedAt);
    restLog(addresses.length, 'addresses');
    restLog('*******************************');
    clearUploadMeasure();
    // loop customers
    const promises = [];
    customers.map(async customer => {
       // matched address and customer
        const matchCustomer = matchedCustomerFromBCToP21(bcCustomer, customer.customer_name);
        if (matchCustomer.length === 0) {
            const matchedCustomerAddress = matchCustomerAddress(addresses, customer.customer_id);
            // if matched address and customer
            if (matchedCustomerAddress.length > 0) {
                const address = matchedCustomerAddress[0];
                const matchAddressContact = matchedCustomerContacts(contacts, customer.customer_name);
                if (matchAddressContact.length > 0) {
                    const contact = matchAddressContact[0];
                    try {
                        const inputData =
                            [
                                {
                                    last_name: contact.last_name,
                                    first_name: contact.first_name,
                                    email: address.email_address,
                                    company: customer.customer_name,
                                    addresses: [
                                        {
                                            last_name: contact.last_name,
                                            first_name: contact.first_name,
                                            city: address.phys_city,
                                            country_code: DEFAULT_COUNTRY_CODE,
                                            state_or_province: matchState(address.phys_state),
                                            address1: address.phys_address1,
                                            postal_code: address.phys_postal_code,
                                            country: address.phys_country,
                                            phone: contact.direct_phone ? contact.direct_phone : '',
                                            address_type: ADDRESS_TYPE
                                        },
                                    ],
                                    attributes: [
                                        {
                                            attribute_id: 1,
                                            attribute_value: customer.customer_name
                                        }
                                    ],
                                    authentication: {
                                        "force_password_reset": true,
                                        "new_password": process.env.TEMP_BC_PASSWORD
                                    },
                                }
                            ];
                        if (!skipServerUpdate) {
                            promises.push(addCustomer(inputData));
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
        }
    });
    fullFillPromises(promises, 'importCustomer');
};

const prepareInventoryImport = async (firstPageOnly = false) => {
    const token = await getToken();
    let modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
        .subtract(4, 'hours').format(DATE_FORMAT);
    let productModifiedAt = '';
    if (firstPageOnly) {
        productModifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(1, 'hours').format(DATE_FORMAT);
    }
    restLog('**********************************');
    const bigCommerceProducts = await getAllBCommerceProducts(firstPageOnly);
    restLog('**********************************');
    restLog('bigCommerceProducts: ', bigCommerceProducts.length);
    restLog('**********************************');
    const products = await getProducts(token, productModifiedAt);
    restLog('**********************************');
    restLog('p21Products: ', products.length);
    restLog('**********************************');
    let inventoryLocation = await getItemInventory(token, modifiedAt);
    inventoryLocation = await getRecentInventoryLocation(token, inventoryLocation);
    restLog('**********************************');
    restLog('inventoryLocation: ', inventoryLocation.length);
    restLog('**********************************');
    let locations = await getLocations(token);

    return {
        bigCommerceProducts,
        products,
        inventoryLocation,
        locations
    }
};

const importInventory = async (skipServerUpdate = false) => {
  restLogTime('importInventory');
  const { bigCommerceProducts, products, inventoryLocation } = await prepareInventoryImport(skipServerUpdate);
  clearUploadMeasure();
  let promises = [];
  products.map(async product => {
      const matchedBigCommerceProducts = getMatchBigCommerceToP21(bigCommerceProducts, product.item_id);
      if (matchedBigCommerceProducts.length > 0) {
          try {
              // api call to upload inventory
              const matchProductInv = matchProductInventory(inventoryLocation, product.inv_mast_uid);
              if (matchProductInv.length > 0) {
                  // get the sum of quantity based p21 locations
                  const totalInventoryForSku = getSumInventory(matchProductInv);
                  const inventoryUpdate = {
                    inventory_level: Math.round((totalInventoryForSku + Number.EPSILON) * 100) / 100,
                    inventory_tracking: "product",
                  };
                  if (!skipServerUpdate) {
                      promises.push(updateInventoryToBigCommerce(matchedBigCommerceProducts[0].id, inventoryUpdate));
                  }
              }
              await sleep(500);
          } catch (error) {
              console.log(error);
          }
      }
  });
  fullFillPromises(promises, 'importInventory');
};

const importInventoryCustomFields = async (skipServerUpdate = false) => {
    restLogTime('importInventoryCustomFields');
    const { bigCommerceProducts, products, inventoryLocation, locations } = await prepareInventoryImport(skipServerUpdate);
    clearUploadMeasure();
    let promises = [];
    products.map(async product => {
        const matchedBigCommerceProducts = getMatchBigCommerceToP21(bigCommerceProducts, product.item_id);
        if (matchedBigCommerceProducts.length > 0) {
            try {
                // api call to upload inventory
                const matchProductInv = matchProductInventory(inventoryLocation, product.inv_mast_uid);
                if (matchProductInv.length > 0) {
                    // get big commerce sku
                    if (!skipServerUpdate) {
                        promises.push(updateProductInventoryCustomFields(
                            matchedBigCommerceProducts[0].id,
                            matchProductInv,
                            locations));
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    });
    fullFillPromises(promises, 'importInventoryCustomFields');
};

const updateInventoryToBigCommerce = async (productId, inventoryUpdateData) => {
    let updatedInventory = null;
    let retryCount = 0;
    while (true) {
        try {
            totalItem++;
            await sleep(5000);
            restLog('updating custom fields..');
            updatedInventory = await Product.updateProduct(productId, inventoryUpdateData);
            successItem++;
            break;
        } catch (error) {
            if (++retryCount === MAX_RETRIES) {
                errorData.push(error.Error);
                failedItem++;
                break;
            }
            await sleep(1000);
        }
    }
    return updatedInventory;
};

// 2248
const updateProductInventoryCustomFields = async (
    productId,
    matchProductInv,
    locations,
    skipServerUpdate = false) => {
    let retryCount = 0;
    const fieldValues = [];
    let fieldResult = null;
    while (true) {
        try {
            const params = convertInventoryToCustomFields(matchProductInv, locations);
            const customFields = await Product.customFields.getCustomFields(productId);
            params.map( async field => {
                const matchField = mapField(customFields.data, field.name);
                if (matchField.length > 0) {
                    if (!skipServerUpdate) {
                        fieldResult = await Product.customFields.updateCustomField(productId, matchField[0].id, field);
                        fieldValues.push(fieldResult.data);
                    }
                } else {
                    if (!skipServerUpdate) {
                        fieldResult = await Product.customFields.addCustomField(productId, field);
                        fieldValues.push(fieldResult.data);
                    }
                }
            });
            break;
        } catch (error) {
            if (++retryCount === MAX_RETRIES) {
                errorData.push(error.Error);
                failedItem++;
                break;
            }
            console.log(error);
        }
    }
    return fieldValues;
};

const fullFillPromises = (promises, timeLabel) => {
    Promise.all(promises).then(() => {
        restLog('Successful Upload(s)', successItem);
        restLog('Failed Upload(s)', failedItem);
        restLog('Total Item(s)', totalItem);
        restLog('Total Error(s)', errorData.length);
        restLog('Total Info(s)', additionalInfo.length);
        restLog('Total Big Commerce Price(s)', bcProductPrice.length);
        restLog('Total Error Report(s)', errorReport.length);
        restLog('All Done...');
        restLogTime(timeLabel, true);
    }).catch(err => {
        console.log('error', err);
    });
};

/**
 *
 * prepareProductsForImport
 * @param skipServerUpdate
 * @returns {Promise<{itemHierarchy: *, bigCommerceProducts: [], shopAllId: number, itemCategories: [], categories: *, bigCommerceCategories: *, products: []}>}
 */
const prepareProductsForImport = async (skipServerUpdate = false) => {
    const token = await getToken();
    let modifiedAt = '';
    if (skipServerUpdate) {
        modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE)
            .subtract(2, 'hours').format(DATE_FORMAT);
    }
    const bigCommerceCategories = await getBigCommerceCategories();
    restLog('**********************************');
    restLog('bigCommerceCategories: ', bigCommerceCategories.length);
    restLog('**********************************');
    const itemHierarchy = await getCategoryHierarchy(token);
    restLog('**********************************');
    restLog('itemHierarchy: ', itemHierarchy.length);
    restLog('**********************************');
    const categories = await getCategory(token);
    restLog('**********************************');
    restLog('categories: ', categories.length);
    restLog('**********************************');
    let shopAllId = getShopAllId(bigCommerceCategories);
    let itemCategories = await getItemCategories(token, skipServerUpdate);
    restLog('**********************************');
    restLog('itemCategories: ', itemCategories.length);
    restLog('**********************************');
    let bigCommerceProducts = await getAllBCommerceProducts(skipServerUpdate);
    restLog('**********************************');
    restLog('bigCommerceProducts: ', bigCommerceProducts.length);
    restLog('**********************************');
    let products = await getProducts(token, modifiedAt);
    restLog('**********************************');
    restLog('p21Products: ', products.length);
    restLog('**********************************');
    return {
        products,
        bigCommerceProducts,
        itemCategories,
        shopAllId,
        categories,
        itemHierarchy,
        bigCommerceCategories,
    }
};

/**
 * importProduct
 *
 * @param skipServerUpdate
 * @returns {Promise<void>}
 */
const importProduct = async (skipServerUpdate = false) => {
    restLogTime('importProduct');
    const {
        products,
        bigCommerceProducts,
        itemCategories,
        shopAllId,
        categories,
        itemHierarchy,
        bigCommerceCategories,
    } = await prepareProductsForImport(skipServerUpdate);
    clearUploadMeasure();
    const promises = [];
    products.map(async product => {
        const matchedBigCommerceProducts = getMatchBigCommerceToP21(bigCommerceProducts, product.item_id);
        if (matchedBigCommerceProducts.length === 0) {
            const bcCategoriesParam = [shopAllId];
            const itemCategoryValue = getItemCategoryByItem(itemCategories, product.inv_mast_uid);
            // console.log(itemCategoryValue);
            if (itemCategoryValue.length > 0) {
                const parentCategory = getParentCategoryByItem(itemHierarchy, itemCategoryValue[0].itemCategoryUid);
                const category = getCategoryById(categories, parentCategory[0].parentCategoryId);
                const matched = matchCategory(bigCommerceCategories, category[0].description);
                if (matched.length > 0) {
                    bcCategoriesParam.push(matched[0].id);
                }
            }
            const productSKU = product.sku || '';
            const productDescription = product.description || '';
            const productName = product.name || '';
            if (productSKU.indexOf(REMOVE_PREFIX) === -1
                && productName.indexOf(REMOVE_PREFIX) === -1
                && productDescription.indexOf(REMOVE_PREFIX) === -1) {
                const productData = {
                    name: product.item_desc,
                    type: PRODUCT_TYPE,
                    description: !product.extended_desc ? product.item_desc : product.extended_desc,
                    price: product.price4 ? product.price4 : Number(product.price1) * PRICE_PERCENT_MARKUP,
                    categories: bcCategoriesParam,
                    availability: PRODUCT_AVAILABILITY,
                    weight: product.weight || 0,
                    height: product.height || 0,
                    width: product.width || 0,
                    depth: product.length || 0,
                    sku: product.item_id,
                    inventory_tracking: 'product',
                };
                if (!skipServerUpdate) {
                    promises.push(addProduct(productData));
                }
            }
        }
    });
    fullFillPromises(promises, 'importProduct');
};

/**
 *
 * @param productId
 * @param param
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const addUpdateProduct = async (productId, param, skipServerUpdate = false) => {
    let updatedProduct = null;
    let retryCount = 0;
    while (true) {
        try {
            totalItem++;
            await sleep(500);
            if (!skipServerUpdate) {
                updatedProduct = await Product.updateProduct(productId, param);
                successItem++;
            }
            break;
        } catch (error) {
            if (error.code === 409) {
                param.name = `${param.name} (${param.sku})`;
                await sleep(500);
                try {
                    if (!skipServerUpdate) {
                        updatedProduct = await Product.updateProduct(productId, param);
                        successItem++;
                    }
                } catch (errorSecondAttempt) {
                    if (error.code === 409) {
                        additionalInfo.push('Item already exists.');
                    }
                }
                return updatedProduct;
            }
            if (++retryCount === MAX_RETRIES) {
                failedItem++;
                errorReport.push(error.Error);
                console.log(error.Error);
                break;
            }
        }
    }
    return updatedProduct;
};

/**
 *
 * @param inputData
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const addCustomer = async (inputData, skipServerUpdate = false) => {
    let addedCustomer = null;
    let retryCount = 0;
    while (true) {
        try {
            totalItem++;
            await sleep(500);
            if (!skipServerUpdate) {
                addedCustomer = await Customer.addCustomer(inputData);
                successItem++;
            }
            break;
        } catch (error) {
            if (++retryCount === MAX_RETRIES) {
                failedItem++;
                errorData.push(error.Error);
                break;
            }
            console.log(error);
        }
    }
    return addedCustomer;
};

/**
 *
 * @param productData
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const addProduct = async (productData, skipServerUpdate = false) => {
    let productResult = null;
    let retryCount = 0;
    restLog(skipServerUpdate, 'addProduct');
    while(true) {
        try {
            totalItem++;
            await sleep(500);
            if (!skipServerUpdate) {
                productResult = await Product.addProduct(productData);
                successItem++;
            }
            break;
        } catch (error) {
            if (error.code === 409) {
                await sleep(500);
                productData.name = `${productData.name} (${productData.sku})`;
                try {
                    if (!skipServerUpdate) {
                        productResult = await Product.addProduct(productData);
                        successItem++;
                    }
                    break;
                } catch (errorSecondDuplicate) {
                    if (errorSecondDuplicate.code === 409) {
                        additionalInfo.push('Item already exists.');
                    }
                }
                return productResult;
            }
            if (++retryCount === MAX_RETRIES){
                failedItem++;
                errorData.push(error.Error);
                console.log(error);
                break;
            }
        }
    }
    return productResult;
};

/***
 *
 * @param body
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const updateCustomer = async (body, skipServerUpdate = false) => {
    let customer = null;
    let retryCount = 0;
    while (true) {
        try {
            totalItem++;
            if (!skipServerUpdate) {
                customer = await Customer.updateCustomer(body);
                successItem++;
            }
            break;
        } catch (error) {
            if (++retryCount === MAX_RETRIES) {
                console.log(error.Error);
                errorData.push(error.Error);
                failedItem++;
                break;
            }
        }
    }
    return customer;
};

module.exports = {
    updateImportedProduct,
    importProduct,
    importCustomer,
    importInventory,
    importInventoryCustomFields,
    clearUploadMeasure,
    getUploadMeasures,
    prepareInventoryImport,
    updateProductInventoryCustomFields,
    addUpdateProduct,
    addProduct,
    addCustomer,
    updateCustomer,
    updateInventoryToBigCommerce,
};
