const {
    ViewCategoryHierarchy,
    ViewCategoryItem,
    ViewInvMaster,
    ViewCustomer,
    ViewContacts,
    ViewAddress,
    ViewInventoryLocation,
    ViewContactLinks,
    ViewLocation,
    ViewOrderHeader,
} = require('../p21/apis');
const bcOrder = require('../big_commerce/apis/order');

const { OPERATOR } = require('../p21/enums');
const Category = require('../big_commerce/apis/category');
const { ViewCategory } = require('../p21/apis');
const _ = require('lodash');

const {
    sleep,
    checkInvalidOrEmptyArray,
    checkArray,
    checkInputString,
    restLogTime,
    restLog,
} = require('../utils/helper');

const {
    DEFAULT_LIMIT,
    DEFAULT_PAGE,
    DEFAULT_SLEEP,
} = require('../utils/constants').pageValues;

const Product = require('../big_commerce/apis/product');
const Customer = require('../big_commerce/apis/customer');

const getCategoryHierarchy = async (token) => {
    restLogTime('categoryHierarchy');
    let categoryHierarchy = [];
    try {
        const filters = [
            {
                type: OPERATOR.NOT_EQUAL,
                field: 'parent_item_category_uid',
                value: 1
            }
        ];
        categoryHierarchy = await ViewCategoryHierarchy(token, filters);
    } catch (error) {
        restLog('Error Getting category Hierarchy', error);
    }
    restLogTime('categoryHierarchy', true);
    return categoryHierarchy;
};

const getItemCategories = async (token, firstPageOnly = false) => {
    restLogTime('getItemCategory');
    let itemCategories = [];
    let done = false;
    let lastNumber = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'inv_mast_uid',
                    value: Number(lastNumber)
                },
            ];
            const productCategory = await ViewCategoryItem(token, filters, 1000);
            itemCategories.push(...productCategory);
            done = productCategory.length === 0 || firstPageOnly === true;
            lastNumber = !done ? productCategory[productCategory.length -1].invMasterUid : 0;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error Getting Item Categories', error);
    }
    restLogTime('getItemCategory', true);
    return itemCategories;
};

const getProducts = async (token, fromDate = '') => {
    restLogTime('getProducts');
    // price1 gt 0 and delete_flag ne 'Y'
    let products = [];
    let done = false;
    let lastNumber = 0;
    let ctrRun = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'price1',
                    value: Number(0)
                },
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'inv_mast_uid',
                    value: Number(lastNumber)
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'delete_flag',
                    value: 'Y'
                },
            ];
            if (checkInputString(fromDate)) {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const productResult = await ViewInvMaster(token, filters, 500);
            if (checkArray(productResult)) {
                products.push(...productResult);
            }
            done = checkInvalidOrEmptyArray(productResult);
            lastNumber = !done ? productResult[productResult.length -1].inv_mast_uid : 0;
            ctrRun++;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting products', error);
    }
    restLogTime('getProducts', true);
    return products;
};

const getAllBCommerceProducts = async (firstPageOnly = false) => {
    restLogTime('getBigCommerceProducts');
    // price1 gt 0 and delete_flag ne 'Y'
    let products = [];
    let done = false;
    let nextLinks = `limit=${DEFAULT_LIMIT}`;
    try {
        do {
            const queryString = nextLinks.replace('?', '');
            const productResult = await Product.getProducts(queryString);
            products.push(...productResult.data);
            if (firstPageOnly) {
                done = true;
            } else if (!productResult.meta.pagination.links.next) {
                done = true;
            } else {
                nextLinks = productResult.meta.pagination.links.next;
            }
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        console.log('Error getting big commerce products', error);
    }
    restLogTime('getBigCommerceProducts', true);
    return products;
};

const getCustomer = async (token, fromDate = '') => {
    restLogTime('getP21Customer');
    let customers = [];
    let done = false;
    let lastNumber = 0;
    let ctrRun = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'customer_id',
                    value: Number(lastNumber)
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'delete_flag',
                    value: 'Y'
                }
            ];
            if (fromDate !== '') {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const customerResult = await ViewCustomer(token, filters, 500);
            if (Array.isArray(customerResult)) {
                customers.push(...customerResult);
            }
            done = checkInvalidOrEmptyArray(customerResult);
            lastNumber = !done ? customerResult[customerResult.length -1].customer_id : 0;
            ctrRun++;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        console.log('Error getting customer', error);
    }
    restLogTime('getP21Customer', true);
    return customers;
};

const getLocations = async (token) => {
    restLogTime('getInventoryLocations');
    let locations = [];
    try {
        const filters = [
            {
                type: OPERATOR.NOT_EQUAL,
                field: 'delete_flag',
                value: 'Y'
            }
        ];
        const locationResult = await ViewLocation(token, filters);
        if (Array.isArray(locationResult)) {
            locations.push(...locationResult);
        }
    } catch (error) {
        restLog('Error getting inventory locations', error);
    }
    restLogTime('getInventoryLocations', true);
   return locations;
};

const getContacts = async (token, fromDate = '') => {
    restLogTime('getContacts');
    let contacts = [];
    let done = false;
    let lastNumber = 0;
    let ctrRun = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'id',
                    value: `${lastNumber}`
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'delete_flag',
                    value: 'Y'
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'email_address',
                    value: null
                },
            ];
            if (checkInputString(fromDate)) {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const customerResult = await ViewContacts(token, filters, 500);
            if (checkArray(customerResult)) {
                contacts.push(...customerResult);
            }
            done = checkInvalidOrEmptyArray(customerResult);
            lastNumber = !done ? customerResult[customerResult.length -1].id : 0;
            ctrRun++;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting contacts', error);
    }
    restLogTime('getContacts', true);
    return contacts;
};



const getAddresses = async (token, fromDate = '') => {
    restLogTime('getAddresses');
    let addresses = [];
    let done = false;
    let lastNumber = 0;
    let ctrRun = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'id',
                    value: Number(lastNumber)
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'email_address',
                    value: null
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'delete_flag',
                    value: 'Y'
                },
            ];
            if (checkInputString(fromDate)) {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const addressResult = await ViewAddress(token, filters, 500);
            if (checkArray(addressResult)) {
                addresses.push(...addressResult);
            }
            done = checkInvalidOrEmptyArray(addressResult);
            lastNumber = !done ? addressResult[addressResult.length -1].id : 0;
            ctrRun++;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting addresses', error);
    }
    restLogTime('getAddresses', true);
    return addresses;
};

const getRecentInventoryLocation = async (token, inventoryLocation) => {
    let inventories = [];
    try {
        const filters = [];
        inventoryLocation.map(invLoc => {
            filters.push(
                {
                    type: OPERATOR.EQUAL,
                    field: 'inv_mast_uid',
                    value: Number(invLoc.inv_mast_uid)
                }
            );
        });
        inventories = await ViewInventoryLocation(token, filters, 100, 'inv_mast_uid', 'or');
    } catch (error) {
        console.log('Error getting item inventory', error);
    }
    return inventories;
};

const getItemInventory = async (token, fromDate = '') => {
    restLogTime('getItemInventory');
    let inventories = [];
    let done = false;
    let lastNumber = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'inv_mast_uid',
                    value: Number(lastNumber)
                },
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'qty_on_hand',
                    value: Number(0)
                },
                {
                    type: OPERATOR.NOT_EQUAL,
                    field: 'delete_flag',
                    value: 'Y'
                },
            ];
            if (fromDate !== '') {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const itemInventoryResult = await ViewInventoryLocation(token, filters, 200);
            if (checkArray(itemInventoryResult)) {
                inventories.push(...itemInventoryResult);
            }
            done = checkInvalidOrEmptyArray(itemInventoryResult);
            lastNumber = !done ? itemInventoryResult[itemInventoryResult.length -1].inv_mast_uid : 0;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting item inventory', error);
    }
    restLogTime('getItemInventory', true);
    return inventories;
};

const getOrders = async (token, forBigCommerce = false, fromDate = '') => {
    restLogTime('getOrders');
    let orders = [];
    let done = false;
    let lastNumber = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'oe_hdr_uid',
                    value: lastNumber
                }
            ];
            if (forBigCommerce) {
                filters.push({
                    type: OPERATOR.NOT_EQUAL,
                    field: 'web_reference_no',
                    value: null
                })
            }
            if (fromDate !== '') {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                });
            }
            const orderResult = await ViewOrderHeader(token, filters);
            if (checkArray(orderResult)) {
                orders.push(...orderResult);
            }
            done = checkInvalidOrEmptyArray(orderResult);
            lastNumber = !done ? orderResult[orderResult.length -1].oe_hdr_uid : 0;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting orders', error);
    }
    restLogTime('getOrders', true);
    return orders;
};

const getContactLinks = async (token, fromDate = '') => {
    restLogTime('getContactLinks');
    let contactLinks = [];
    let done = false;
    let lastNumber = 0;
    try {
        do {
            const filters = [
                {
                    type: OPERATOR.GREATER_THAN,
                    field: 'id',
                    value: `${lastNumber}`
                },
                {
                    type: OPERATOR.CONTAINS,
                    field: 'title',
                    value: 'A/P'
                },
            ];
            if (fromDate !== '') {
                filters.push({
                    type: OPERATOR.GREATER_THAN_EQUAL,
                    field: 'date_last_modified',
                    value: `datetime'${fromDate}'`
                })
            }
            const contactLinksResult = await ViewContactLinks(token, filters, 200);
            if (checkArray(contactLinksResult)) {
                contactLinks.push(...contactLinksResult);
            }
            done = checkInvalidOrEmptyArray(contactLinksResult);
            lastNumber = !done ? contactLinksResult[contactLinksResult.length -1].id : 0;
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting contact links', error);
    }
    restLogTime('getContactLinks', true);
    return contactLinks;
};

const getBigCommerceCustomer = async (queryString = '', firstPageOnly = false) => {
    restLogTime('getBigCommerceCustomer');
    // price1 gt 0 and delete_flag ne 'Y'
    let customers = [];
    let done = false;
    let page = DEFAULT_PAGE;
    let limit = DEFAULT_LIMIT;
    let nextLinks = `limit=${limit}&page=${page}${queryString}`;
    try {
        do {
            queryString = nextLinks.replace('?', '');
            const customerResult = await Customer.getCustomers(queryString);
            customers.push(...customerResult.data);
            if (firstPageOnly === true) {
                done = true;
            } else if (Array.isArray(customerResult.data) && customerResult.data.length === 0) {
                done = true;
            } else {
                nextLinks = `limit=${limit}&page=${++page}`;
            }
            await sleep(DEFAULT_SLEEP);
        } while (!done);
    } catch (error) {
        restLog('Error getting big commerce customers', error);
    }
    restLogTime('getBigCommerceCustomer', true);
    return customers;
};

const getBigCommerceCategories = async () => {
    restLogTime('getBigCommerceCategories');
    let categories = [];
    try {
        categories = await Category.getCategories();
        categories = categories.data.map(category => {
            return {
                id: category.id,
                name: category.name
            }
        });
    } catch (error) {
        restLog('Eror getting bigcommerce categories', error);
    }
    restLogTime('getBigCommerceCategories', true);
    return categories;
};

const getCategory = async (token) => {
    restLogTime('getCategory');
    let categories = [];
    try {
        categories = await ViewCategory(token);
    } catch (error) {
        restLog('Error getting p21 categories', error);
    }
    restLogTime('getCategory', true);
    return categories;
};

const getOrderItems = async (orderId, items, pickOrderDetails) => {
    restLogTime('getOrderItems');
    const orderItemParam = [];
    try {
        const orderItems = await bcOrder.getOrderProducts(orderId);
        restLog(pickOrderDetails, 'pick order details');
        for (let key in orderItems) {
            const orderItem = orderItems[key];
            const mapOrderItem = _.filter(items, (item) => {
                return item.item_id === orderItem.sku;
            });
            if (mapOrderItem.length > 0) {
                const mapPickOrderDetails = _.filter(pickOrderDetails, (pickOrderDetail) => {
                    return pickOrderDetail.inv_mast_uid === mapOrderItem[0].inv_mast_uid;
                });
                restLog(mapPickOrderDetails);
                if (mapPickOrderDetails.length > 0) {
                    orderItemParam.push(
                        {
                            order_product_id: orderItem.id,
                            quantity: mapPickOrderDetails[0].ship_quantity
                        }
                    );
                }
            }
        }
    } catch (error) {
        restLog('Error getting order items', error);
    }
    restLogTime('getOrderItems', true);
    return orderItemParam;
};

module.exports = {
    getCategoryHierarchy,
    getItemCategories,
    getProducts,
    getAllBCommerceProducts,
    getContacts,
    getAddresses,
    getCustomer,
    getItemInventory,
    getRecentInventoryLocation,
    getContactLinks,
    getBigCommerceCustomer,
    getBigCommerceCategories,
    getLocations,
    getOrders,
    getCategory,
    getOrderItems,
};
