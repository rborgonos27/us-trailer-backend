const bcOrder = require('../big_commerce/apis/order');
const { getToken } = require('../utils/p21');
const P21PickList = require('../p21/apis/view_picklist');
const P21PickListDetails = require('../p21/apis/view_picklist_details');
const P21InvMaster = require('../p21/apis/view_inv_master');
const { OPERATOR } = require('../p21/enums');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const { DATE_FORMAT } = require('../utils/constants').P21;
const { getCustomer, getBigCommerceCustomer, getOrders, getOrderItems } = require('./service');
const { matchedHelper } = require('./helper');
const { addCustomer } = require('../service/customer');
const { createOrder } = require('../service/order');

const { QUERY_DATE_FORMAT } =  require('../utils/constants').BigCommerce;
const { MOMENT_LAST_HOUR, MOMENT_LAST_MINUTE } =  require('../utils/constants').General;
const { BigCommerceOrderStatus, BigCommerceShippingMethod } = require('../utils/constants');
const { getDataFromArray, restLog, restLogTime } = require('../utils/helper');

/***
 *
 * @returns {Promise<void>}
 */
const syncCustomerDataToP21 = async (skipServeUpdate = false) => {
    const dateMax = moment().format(QUERY_DATE_FORMAT);
    const dateMin = moment().subtract(MOMENT_LAST_HOUR, 'hours').format(QUERY_DATE_FORMAT);
    const queryString = `&date_created:max=${dateMax}&date_created:min=${dateMin}`;
    const customers = await getBigCommerceCustomer(queryString, true);

    const token = await getToken();
    // -> check customer to p21
    const p21Customer = await getCustomer(token);
    customers.map(async customer => {
        // - -> if exists do nothing
        const matchCustomer = matchedHelper.matchedCustomerFromP21ToBc(p21Customer, customer.company);
        if (matchCustomer.length === 0) {
            if (!skipServeUpdate) {
                await addCustomer(customer.id);
            }
        }
    });
};

/***
 *
 * @returns {Promise<void>}
 */
const syncOrderDataToP21 = async (skipServerUpdate = false) => {
    /* istanbul ignore next */
    // get orders from certain period
    let minDate = moment_tz().utc();
    minDate = moment(minDate).subtract(MOMENT_LAST_HOUR, 'hours');
    const p21MinDate = moment_tz().tz(process.env.P21_TIMEZONE).subtract(MOMENT_LAST_HOUR, 'hours');
    const dateMax = moment_tz().utc().format(DATE_FORMAT);
    const dateMin = minDate.format(DATE_FORMAT);
    restLog(minDate, `${dateMin},${dateMax}`);
    const token = await getToken();
    const p21Orders = await getOrders(token, p21MinDate);
    const orders = await bcOrder.getAllOrders(`min_date_created=${dateMin}&max_date_created=${dateMax}`);
    if (orders) {
        orders.map(async order => {
            const matchOrder = matchedHelper.matchedOrderFromP21ToBc(p21Orders, order.id);
            if (matchOrder.length === 0) {
                if (!skipServerUpdate) {
                    await createOrder(order.id);
                }
            }
        });
    }
};

/***
 *
 * @param token
 * @param orderNo
 * @returns {Promise<{pickTicket: {}, pickTicketDetails: []}>}
 */
const getPickTicketInfo = async (token, orderNo) => {
    let pickTicket = {};
    let pickTicketDetails = [];
    try {
        let filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'order_no',
                value: orderNo
            },
            {
                type: OPERATOR.NOT_EQUAL,
                field: 'invoice_no',
                value: null
            }
        ];
        const orderBy = encodeURI('date_last_modified desc');
        pickTicket = await P21PickList(token, filters, 1, orderBy);
        pickTicket = Array.isArray(pickTicket) && pickTicket.length > 0 ? pickTicket[0] : null;
        const pickTicketNo = pickTicket ? pickTicket.pick_ticket_no : 0;
        if (pickTicketNo === 0) {
            return {
                pickTicket: {},
                pickTicketDetails
            }
        }
        filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'pick_ticket_no',
                value: Number(pickTicketNo)
            }
        ];
        pickTicketDetails = await P21PickListDetails(token, filters);
    } catch (error) {
        restLog('Error getting pick ticket info', error);
    }
    return {
        pickTicket,
        pickTicketDetails
    }
};

/***
 * create OrderShipment
 *
 * @param order
 * @param token
 * @returns {Promise<{orderShipment: *, isPartial: boolean}>}
 */
const createOrderShipment = async (order, token, pickTicketInfo) => {
    /* istanbul ignore next */
    const orderId = order.web_reference_no;
    const deliveryInstruction = order.delivery_instructions;
    const orderShipmentParam = {};
    const shippingAddress = await bcOrder.getOrderShipmentAddresses(orderId);
    // get pick ticket info
    const { pickTicket, pickTicketDetails } =  pickTicketInfo;
    const isPartial = isPartiallyPickedUp(pickTicketDetails);
    const itemQuery = pickTicketDetails.map(item => {
        return {
            type: OPERATOR.EQUAL,
            field: 'inv_mast_uid',
            value: item.inv_mast_uid
        };
    });
    const p21Item = await P21InvMaster(token, itemQuery, 100, 'item_id', 'or');
    restLog(p21Item);
    // get pick ticket details
    if (Array.isArray(shippingAddress) && shippingAddress.length > 0) {
        orderShipmentParam.order_address_id = shippingAddress[0].id;
    }
    orderShipmentParam.comments = deliveryInstruction || '';
    // orderShipmentParam.shipping_provider = pickTicket.carrier_id ? carrier[pickTicket.carrier_id] : ""; // <-- commented out for now
    orderShipmentParam.tracking_number = pickTicket ? pickTicket.tracking_no : '';
    orderShipmentParam.items = await getOrderItems(orderId, p21Item, pickTicketDetails);
    restLog(orderShipmentParam);
    restLog(orderShipmentParam.items);
    const orderShipment = await bcOrder.createOrderShipment(orderId, orderShipmentParam);
    return {
        orderShipment,
        isPartial,
    }
};

/***
 *
 * @param pickTicketDetails
 * @returns {boolean}
 */
const isPartiallyPickedUp = (pickTicketDetails) => {
    let isPartial = false;
    for (let key in pickTicketDetails) {
        const pickTicketDetail =  pickTicketDetails[key];
        if (pickTicketDetail.ship_quantity !== pickTicketDetail.qty_requested) {
            isPartial = true;
            break;
        }
    }
    return isPartial;
};

const processPartialShipped = async (skipServerUpdate, token, orderNo, order, orderBodyRequest) => {
    if (!skipServerUpdate) {
        const pickTicketInfo = await getPickTicketInfo(token, orderNo);
        if (Object.keys(pickTicketInfo.pickTicket).length > 0) {
            const orderShipments = await createOrderShipment(order, token, pickTicketInfo);
            const { isPartial } = orderShipments;
            if (isPartial) {
                restLog('==== processing partial shipped', order.web_reference_no);
                orderBodyRequest.status_id = BigCommerceOrderStatus.PARTIALLY_SHIPPED;
            }
        }
    }
    return orderBodyRequest;
};

const processShipped = async (skipServerUpdate, token, orderNo, order, orderBodyRequest) => {
    if (!skipServerUpdate) {
        const pickTicketInfo = await getPickTicketInfo(token, orderNo);
        const orderShipments = await createOrderShipment(order, token, pickTicketInfo);
        const { isPartial } = orderShipments;
        if (isPartial) {
            restLog('==== processing partial shipped', order.web_reference_no);
            orderBodyRequest.status_id = BigCommerceOrderStatus.PARTIALLY_SHIPPED;
        } else {
            restLog('==== processing shipped', order.web_reference_no);
            orderBodyRequest.status_id = BigCommerceOrderStatus.SHIPPED;
        }
    }
    return orderBodyRequest;
};

/**
 *
 * @returns {Promise<void>}
 */
const syncP21OrderToBigCommerce = async (skipServerUpdate = false) => {
    /* istanbul ignore next */
    restLogTime('syncP21OrderToBigCommerce');
    const token = await getToken();
    const modifiedAt = moment_tz().tz(process.env.P21_TIMEZONE).subtract(MOMENT_LAST_MINUTE, 'minutes').format(DATE_FORMAT);
    const p21Orders = await getOrders(token, true, modifiedAt);
    let orderBodyRequest = {};
    p21Orders.map(async order => {
        try {
            let orderShipment = await bcOrder.getShippingAddresses(order.web_reference_no);
            let bcOrderData = await bcOrder.getOrders(order.web_reference_no);
            orderShipment = getDataFromArray(orderShipment);
            const orderNo = order.order_no;

            if (order.validation_status === 'Approved' &&
                bcOrderData.status_id !== BigCommerceOrderStatus.SHIPPED &&
                bcOrderData.status_id !== BigCommerceOrderStatus.COMPLETED) {
                if (order.cancel_flag === 'Y') {
                    restLog('==== processing cancelled');
                    orderBodyRequest.status_id = BigCommerceOrderStatus.CANCELLED;
                } else if (order.completed === 'Y' && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_PICKUP) {
                    restLog('==== processing pickup to store');
                    const { pickTicketDetails } = await getPickTicketInfo(token, orderNo);
                    const isPartialPickUp = isPartiallyPickedUp(pickTicketDetails);
                    if (!isPartialPickUp) {
                        orderBodyRequest.status_id = BigCommerceOrderStatus.COMPLETED;
                    }
                }  else if (order.completed === 'N' && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_SHIPMENT) {
                    orderBodyRequest = await processPartialShipped(skipServerUpdate, token, orderNo, order, orderBodyRequest);
                } else if (order.completed === 'N' && bcOrderData.status_id === BigCommerceOrderStatus.PARTIALLY_SHIPPED) {
                    orderBodyRequest = await processPartialShipped(skipServerUpdate, token, orderNo, order, orderBodyRequest);
                }  else if (order.completed === 'Y' && bcOrderData.status_id === BigCommerceOrderStatus.PARTIALLY_SHIPPED) {
                    orderBodyRequest = await processShipped(skipServerUpdate, token, orderNo, order, orderBodyRequest);
                } else if (order.completed === 'Y' && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_SHIPMENT) {
                    orderBodyRequest = await processShipped(skipServerUpdate, token, orderNo, order, orderBodyRequest);
                }
                if (orderShipment.shipping_method === BigCommerceShippingMethod.PICKUP_IN_CHICAGO
                    && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_FULFILLMENT) {
                    orderBodyRequest.status_id = BigCommerceOrderStatus.AWAITING_PICKUP;
                } else if (orderShipment.shipping_method === BigCommerceShippingMethod.PICKUP_IN_MILWAUKEE
                    && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_FULFILLMENT) {
                    orderBodyRequest.status_id = BigCommerceOrderStatus.AWAITING_PICKUP;
                } else if (orderShipment.shipping_method === BigCommerceShippingMethod.FREE_SHIPPING
                    && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_FULFILLMENT) {
                    orderBodyRequest.status_id = BigCommerceOrderStatus.AWAITING_SHIPMENT;
                }  else if (orderShipment.shipping_method === BigCommerceShippingMethod.US_TRAILER_DELIVERY
                    && bcOrderData.status_id === BigCommerceOrderStatus.AWAITING_FULFILLMENT) {
                    orderBodyRequest.status_id = BigCommerceOrderStatus.AWAITING_SHIPMENT;
                }
                restLog('********************');
                restLog(orderBodyRequest, order.web_reference_no);
                restLog('********************');
                if (!skipServerUpdate && Object.keys(orderBodyRequest).length > 0) {
                    orderBodyRequest.staff_notes = `Prophet21 Order No: ${orderNo}`;
                    const orderRequest = await bcOrder.updateOrder(order.web_reference_no, orderBodyRequest);
                    restLog(orderRequest);
                }
            }
        } catch (error) {
            restLog('Error sync p21 Order to Big Commerce', error);
        }
    });
    restLogTime('syncP21OrderToBigCommerce', true);
};

module.exports = {
    syncCustomerDataToP21,
    syncOrderDataToP21,
    syncP21OrderToBigCommerce,
};
