const { options, requestService } = require('../helper');
const { ACCOUNTING_EXCHANGE_RATES } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(`${ACCOUNTING_EXCHANGE_RATES}?format=json`, credentials, 'GET');
    return requestService(config);
};
