const { options, requestService, writeOption } = require('../helper');
const { ENTITY_ADDRESS } = require('../enums').API;

/***
 *
 * @param token
 * @param addressId
 * @returns {Promise<unknown>}
 */
const getAddress = (token, addressId = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const endPoint = `${ENTITY_ADDRESS}${addressId ? addressId: ''}?format=json`;
    const config = options(endPoint, credentials, 'GET');
    return requestService(config);
};

/**
 *
 * @param token
 * @param body
 * @returns {Promise<unknown>}
 */
const saveAddress = async (token, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const endpoint = `${ENTITY_ADDRESS}`;
    const config = writeOption(endpoint, credentials, 'POST', body);
    return await requestService(config);
};

/***
 *
 * @param token
 * @param body
 * @returns {Promise<unknown>}
 */
const updateAddress = async (token, addressId, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const endpoint = `${ENTITY_ADDRESS}${addressId}`;
    const config = writeOption(endpoint, credentials, 'PUT', body);
    return await requestService(config);
};

module.exports = {
    getAddress,
    saveAddress,
    updateAddress,
};

