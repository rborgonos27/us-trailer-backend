const { options, requestService } = require('../helper');
const { AUTH } = require('../enums').API;

/***
 * Authentication method
 *
 * @param username
 * @param password
 * @returns {Promise<unknown>}
 */
module.exports = (username, password) => {
    const credentials = {
        username,
        password
    };
    const config = options(`${AUTH}?format=json`, credentials, 'POST');
    return requestService(config);
};