const { options, requestService } = require('../helper');
const { CONFIG } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(CONFIG, credentials, 'GET');
    return requestService(config);
};
