const { options, requestService, writeOption, writeRequestService } = require('../helper');
const { ENTITY_CONTACTS } = require('../enums').API;

/***
 *
 * @param token
 * @param contactId
 * @returns {Promise<unknown>}
 */
const getContact = async (token, contactId = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const endPoint = `${ENTITY_CONTACTS}${contactId ? contactId: ''}?format=json`;
    const config = options(endPoint, credentials, 'GET');
    return await requestService(config);
};


/**
 *
 * @param token
 * @param body
 * @returns {Promise<unknown>}
 */
const saveContact = async (token, body) => {
  const credentials = {
      Authorization: `Bearer ${token}`,
  };
  credentials['Content-Type'] = 'application/json';
  credentials['Accept'] = 'application/json';
  const endpoint = `${ENTITY_CONTACTS}?format=json`;
  const config = writeOption(endpoint, credentials, 'POST', body);
  return await writeRequestService(config);
};

module.exports = {
    getContact,
    saveContact,
};