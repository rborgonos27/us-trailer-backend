const { options, requestService, writeOption, writeRequestService } = require('../helper');
const { ENTITY_CUSTOMER } = require('../enums').API;

/***
 *
 * @param token
 * @param params
 * @returns {Promise<unknown>}
 */
const getCustomer = (token, params = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const customerCompanyId = params ? `${params.companyId}_${params.customerId}` : '';
    const endPoint = `${ENTITY_CUSTOMER}${customerCompanyId}?format=json`;
    const config = options(endPoint, credentials, 'GET');
    return requestService(config);
};

/***
 *
 * @param token
 * @param body
 * @returns {Promise<unknown>}
 */
const saveCustomer = async (token, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    credentials['Content-Type'] = 'application/json';
    credentials['Accept'] = 'application/json';
    const config = writeOption(`${ENTITY_CUSTOMER}?format=json`, credentials, 'POST', body);
    return await writeRequestService(config);
};

/**
 *
 * @param token
 * @param customerId
 * @param body
 * @returns {Promise<unknown>}
 */
const updateCustomer = async (token, customerId, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    credentials['Content-Type'] = 'application/json';
    credentials['Accept'] = 'application/json';
    const config = writeOption(ENTITY_CUSTOMER, credentials, 'PUT', body);
    return await requestService(config);
};

module.exports = {
    getCustomer,
    saveCustomer,
    updateCustomer,
};
