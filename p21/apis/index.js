const AccountingExchangeRates = require('./accounting_exchange_rates');
const Auth = require('./auth');
const Config = require('./config');
const Customer = require('./customer');
const Contacts = require('./contacts');
const InventoryAdjustments = require('./inventory_adjustments');
const InventoryPartScan = require('./inventory_part_scan');
const Parts = require('./parts');
const PartsV2 = require('./parts_v2');
const PurchaseOrder = require('./purchase_orders');
const SalesOrders = require('./sales_orders');
const Services = require('./services');
const Vendor = require('./vendors');
const ViewCategory = require('./view_category');
const ViewCategoryItem = require('./view_category_item');
const ViewCategoryHierarchy = require('./view_category_hierarchy');
const ViewInvMaster = require('./view_inv_master');
const ViewCustomer = require('./view_customer');
const ViewContacts = require('./view_contacts');
const ViewAddress = require('./view_address');
const ViewInventoryLocation = require('./view_inventory_location');
const ViewContactLinks = require('./view_contacts_links');
const ViewLocation = require('./view_location');
const ViewOrderHeader = require('./view_order_header');

module.exports = {
    AccountingExchangeRates,
    Auth,
    Config,
    Customer,
    Contacts,
    InventoryAdjustments,
    InventoryPartScan,
    Parts,
    PartsV2,
    PurchaseOrder,
    SalesOrders,
    Services,
    Vendor,
    ViewCategory,
    ViewCategoryItem,
    ViewCategoryHierarchy,
    ViewInvMaster,
    ViewCustomer,
    ViewContacts,
    ViewAddress,
    ViewInventoryLocation,
    ViewContactLinks,
    ViewLocation,
    ViewOrderHeader
};