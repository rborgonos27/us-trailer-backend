const { options, requestService } = require('../helper');
const { INVENTORY_ADJUSTMENTS } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(INVENTORY_ADJUSTMENTS, credentials, 'GET');
    return requestService(config);
};
