const { options, requestService } = require('../helper');
const { INVENTORY_PART_SCAN } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(INVENTORY_PART_SCAN, credentials, 'GET');
    return requestService(config);
};
