const { options, requestService, getQueryString, writeOption } = require('../helper');
const { INVENTORY_PARTS } = require('../enums').API;

const extendEndpoint = (attributes) => {
    if (!attributes || !attributes.itemId) return '';
    return `${attributes.itemId}${attributes.type ? `/${attributes.type}` : ''}`;
};

/***
 *
 * @param token
 * @param attributes
 * @param filters
 * @param ReferenceId
 * @returns {Promise<*>}
 */
const getParts = async (token, attributes = null, filters = [], ReferenceId = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = getQueryString(filters);
    const jsonFormat = queryString === '' ? '?format=json' : '&format=json';
    const endPoint = `${INVENTORY_PARTS}${extendEndpoint(attributes)}${queryString}${jsonFormat}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config);
    return Array.isArray(result) ? result.map(data => mapPart(data, ReferenceId)) : mapPart(result, ReferenceId);
};

/**
 *
 * @param token
 * @returns {Promise<void>}
 */
const updateParts = async (token, itemId, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`
    };
    credentials['Content-Type'] = 'application/json';
    const endPoint = `${INVENTORY_PARTS}/${itemId}`;
    const config = writeOption(endPoint, credentials, 'PUT', body);
    return await requestService(config);
};

module.exports = {
  getParts,
  updateParts,
};

/***
 *
 * @param data
 * @param ReferenceId
 * @returns {*}
 */
const mapPart = (data, ReferenceId) => {
     const results = {
        UnitsOfMeasure,
        LocationSuppliers,
        Lot,
        LocationMSPs,
        Service,
        ServiceContracts,
        Notes,
        AltCodes,
        ItemId,
        ItemDesc,
        Delete,
        Weight,
        NetWeight,
        Cube,
        CatchWeightIndicator,
        PurchasingWeight,
        ClassId1,
        ClassId2,
        ClassId3,
        ClassId4,
        ClassId5,
        Serialized,
        ShortCode,
        TrackLots,
        Price1,
        Price2,
        Price3,
        Price4,
        Price5,
        Price6,
        Price7,
        Price8,
        Price9,
        Price10,
        SalesPricingUnit,
        SalesPricingUnitSize,
        PurchasePricingUnit,
        PurchasePricingUnitSize,
        ExtendedDesc,
        CommissionClassId,
        OtherChargeItem,
        DefaultSellingUnit,
        DefaultPurchasingUnit,
        InvMastUid,
        Inactive,
        DeleteFlag,
        ItemTermsDiscountPct,
        Keywords,
        BaseUnit,
        CurrencyId,
        OverrideSpecificCosting,
        ParkerProductCd,
        UseRevisionsFlag,
        Length,
        Width,
        Height,
        SalesTaxClass
    } = data;
    if (data.Locations) {
        results.Locations = mapLocation(data.Locations);
    }
    results.ReferenceId = ReferenceId;
    return results;
};

/***
 *
 * @param data
 * @returns {*}
 */
const mapLocation = (data) => {
  return data.list.map(inventory => mapInventory(inventory));
};

/***
 *
 * @param data
 * @returns {*}
 */
const mapInventory = (data) => {
    return {
        ItemId,
        LocationId,
        QtyOnHand,
        Sellable,
        MovingAverageCost,
        StandardCost,
        ProtectedStockQty,
        InvMin,
        InvMax,
        SafetyStock,
        Stockable,
        ReplenishmentLocation,
        ProductGroupId,
        NoCharge,
        Price1,
        Price2,
        Price3,
        Price4,
        Price5,
        Price6,
        Price7,
        Price8,
        Price9,
        Price10,
        ReplenishmentMethod,
        TrackBins,
        PrimaryBin,
        Requisition,
        Buy,
        Make,
        PeriodsToSupplyMin,
        PeriodsToSupplyMax,
        Discontinued,
        AllowDsDiscontinuedItems,
        AllowSpDiscontinuedItems,
        MaxLiability,
        MaxTransferQty,
    } = data;
};
