const { options, requestService } = require('../helper');
const { V2_PARTS } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(V2_PARTS, credentials, 'GET');
    return requestService(config);
};
