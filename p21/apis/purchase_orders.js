const { options, requestService } = require('../helper');
const { PURCHASE_ORDER } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(PURCHASE_ORDER, credentials, 'GET');
    return requestService(config);
};
