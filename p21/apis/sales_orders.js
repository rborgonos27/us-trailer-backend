const { writeOption, writeRequestService } = require('../helper');
const { SALES_ORDERS } = require('../enums').API;

/**
 *
 * @param token
 * @param body
 * @returns {Promise<unknown>}
 */
const saveOrder = (token, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    credentials['Content-Type'] = 'application/json';
    credentials['Accept'] = 'application/json';
    const config = writeOption(`${SALES_ORDERS}?format=json`, credentials, 'POST', body);
    return writeRequestService(config);
};

/**
 *
 * @param token
 * @param orderNo
 * @param body
 * @returns {Promise<unknown>}
 */
const updateOrder = (token, orderNo, body) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    credentials['Content-Type'] = 'application/json';
    credentials['Accept'] = 'application/json';
    const config = writeOption(`${SALES_ORDERS}${orderNo}?format=json`, credentials, 'POST', body);
    return writeRequestService(config);
};

module.exports = {
    saveOrder,
    updateOrder,
};
