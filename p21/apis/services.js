const { options, requestService } = require('../helper');
const { SERVICES } = require('../enums').API;

/***
 *
 * @param token
 * @returns {Promise<unknown>}
 */
module.exports = (token) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const config = options(SERVICES, credentials, 'GET');
    return requestService(config);
};
