const { options, requestService } = require('../helper');
const { VENDORS } = require('../enums').API;

/***
 *
 * @param token
 * @param params
 * @returns {Promise<unknown>}
 */
module.exports = (token, params  = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const customerVendorId = params ? `${params.companyId}_${params.vendorId}` : '';
    const endPoint = `${VENDORS}${customerVendorId}?format=json`;
    const config = options(endPoint, credentials, 'GET');
    return requestService(config);
};
