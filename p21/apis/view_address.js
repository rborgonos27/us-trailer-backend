const { options, requestService, getQueryString } = require('../helper');
const { ADDRESS } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @returns {Promise<*>}
 */
module.exports = async (token, filters = [], top= 100, order_by='id') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
        : '?';
    const select = 'id,name,mail_address1,mail_address2,phys_city,phys_address2,phys_state,phys_address1,phys_postal_code,phys_country,email_address';
    const endPoint = `${ADDRESS}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

