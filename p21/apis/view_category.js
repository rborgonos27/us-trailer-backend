const { options, requestService } = require('../helper');
const { CATEGORY } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param params
 * @returns {Promise<*>}
 */
module.exports = async (token, params = null) => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const endPoint = `${CATEGORY}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'xml');
    const categories = result.feed.entry.map(data => {
        return mapCategory(data);
    });
    return categories
};

/***
 *
 * @param data
 * @returns {{name: (*), description: (*), id: *}}
 */
const mapCategory = (data) => {
   const desc = data.content['m:properties']['d:item_category_desc'];
   const name = data.content['m:properties']['d:item_category_id'];
   return {
     id: data.content['m:properties']['d:item_category_uid']['$t'],
     name: typeof (name) === 'object' ? name['$t'] : name,
     description: typeof (desc) === 'object' ? desc['$t'] : desc,
   };
};