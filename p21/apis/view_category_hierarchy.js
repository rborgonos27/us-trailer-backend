const { options, requestService, getQueryString } = require('../helper');
const { ITEM_CATEGORY_HIERARCHY } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @returns {Promise<{childCategoryId, parentCategoryId: *}[]|null|{childCategoryId: *, parentCategoryId: *}[]>}
 */
module.exports = async (token, filters = []) => {
    let entries = [];
    try {
        if (filters.length === 0) {
            return null;
        }
        const credentials = {
            Authorization: `Bearer ${token}`,
        };
        const queryString = getQueryString(filters, 'and', '$filter');
        const endPoint = `${ITEM_CATEGORY_HIERARCHY}${queryString}`;
        const config = options(endPoint, credentials, 'GET');
        const result = await requestService(config, 'xml');
        entries = result.feed.entry ? result.feed.entry : [];
        if (!Array.isArray(entries)) {
            return [mapCategoryHierarchy(entries)];
        }
        const categories = entries.map(data => {
            return mapCategoryHierarchy(data);
        });
        return categories
    } catch (error) {
        console.log(entries, error);
    }
};

/**
 *
 * @param data
 * @returns {{childCategoryId: *, parentCategoryId: *}}
 */
const mapCategoryHierarchy = (data) => {
    return {
        itemCategoryHierarchyUid: data.content['m:properties']['d:item_category_hierarchy_uid']['$t'],
        parentCategoryId: data.content['m:properties']['d:parent_item_category_uid']['$t'],
        childCategoryId: data.content['m:properties']['d:child_item_category_uid']['$t'],
        sequenceNo: data.content['m:properties']['d:sequence_no']['$t'],
    };
};