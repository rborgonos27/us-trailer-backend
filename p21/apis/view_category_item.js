const { options, requestService, getQueryString } = require('../helper');
const { ITEM_CATEGORY } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @returns {Promise<{displayDesc: (*), itemCategoryUid, id: *, invMasterUid}[]|null|{displayDesc: (*), itemCategoryUid: *, id: *, invMasterUid: *}[]>}
 */
module.exports = async (token, filters = [], top = 100, order_by='inv_mast_uid') => {
    let entries = [];
    try {
        const credentials = {
            Authorization: `Bearer ${token}`,
        };
        const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
            : '?';
        const endPoint = `${ITEM_CATEGORY}${queryString}$format=json&$top=${top}&$orderby=${order_by}&select=item_category_uid,inv_mast_uid`;
        const config = options(endPoint, credentials, 'GET');
        const result = await requestService(config);
        entries = result.value ? result.value : [];
        if (!Array.isArray(entries)) {
            return [mapCategoryItem(entries)];
        }
        const categories = entries.map(data => {
            return mapCategoryItem(data);
        });
        return categories
    } catch (error) {
        console.log(entries, error);
    }
};

/***
 *
 * @param data
 * @returns {{displayDesc: (*), itemCategoryUid: *, id: *, invMasterUid: *}}
 */
const mapCategoryItem = (data) => {
    return {
        itemCategoryUid: data.item_category_uid,
        invMasterUid: Number(data.inv_mast_uid),
    };
};