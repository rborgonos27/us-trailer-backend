const { options, requestService, getQueryString } = require('../helper');
const { CONTACTS_LINKS } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @returns {Promise<*>}
 */
module.exports = async (token, filters = [], top= 100, order_by='id') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
        : '?';
    const select = 'id,link_type,last_name,first_name,name';
    const endPoint = `${CONTACTS_LINKS}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

