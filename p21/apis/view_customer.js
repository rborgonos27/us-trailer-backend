const { options, requestService, getQueryString } = require('../helper');
const { CUSTOMER } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @returns {Promise<*>}
 */
module.exports = async (token, filters = [], top= 100, order_by='customer_id') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
        : '?';
    const select = 'customer_id,customer_name,company_id,delete_flag,date_last_modified';
    const endPoint = `${CUSTOMER}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    if (!result.value) {
        throw new Error(`API Error: ${result['odata.error']['message']['value']}`);
    }
    return result.value;
};

