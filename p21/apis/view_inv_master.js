const { options, requestService, getQueryString } = require('../helper');
const { INV_MASTER } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @returns {Promise<{displayDesc: *, itemCategoryUid: *, id: *, invMasterUid: *}[]|null>}
 */
module.exports = async (token, filters = [], top = 100, order_by='inv_mast_uid', logic = 'and') => {
    let entries = [];
    try {
        if (filters.length === 0) {
            return null;
        }
        const credentials = {
            Authorization: `Bearer ${token}`,
        };
        const queryString = getQueryString(filters, logic, '$filter');
        const select = 'inv_mast_uid,item_id,item_desc,delete_flag,date_last_modified,extended_desc,weight,width,height,length,price1,price4,default_selling_unit';
        const endPoint = `${INV_MASTER}${queryString}&$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
        const config = options(endPoint, credentials, 'GET');
        const result = await requestService(config);
        return result.value;
    } catch (error) {
        console.log(entries, error);
    }
};

