const { options, requestService, getQueryString } = require('../helper');
const { INVENTORY_LOCATION } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @param logic
 * @returns {Promise<*>}
 */
module.exports = async (token, filters = [], top= 100, order_by='inv_mast_uid', logic = 'and') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, logic, '$filter')}&`
        : '?';
    const select = 'location_id,qty_on_hand,inv_mast_uid,delete_flag,date_last_modified';
    const endPoint = `${INVENTORY_LOCATION}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

