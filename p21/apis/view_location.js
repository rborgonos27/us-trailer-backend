const { options, requestService, getQueryString } = require('../helper');
const { LOCATION } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @returns {Promise<{displayDesc: *, itemCategoryUid: *, id: *, invMasterUid: *}[]|null>}
 */
module.exports = async (token, filters = [], top = 100, order_by='default_branch_id') => {
    let entries = [];
    try {
        if (filters.length === 0) {
            return null;
        }
        const credentials = {
            Authorization: `Bearer ${token}`,
        };
        const queryString = getQueryString(filters, 'and', '$filter');
        const select = 'default_branch_id,company_id,location_id,location_name';
        const endPoint = `${LOCATION}${queryString}&$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
        const config = options(endPoint, credentials, 'GET');
        const result = await requestService(config);
        return result.value;
    } catch (error) {
        console.log(entries, error);
    }
};

