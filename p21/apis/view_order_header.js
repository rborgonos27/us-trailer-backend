const { options, requestService, getQueryString } = require('../helper');
const { ORDER_HEADER } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @returns {Promise<*>}
 */
module.exports = async (token, filters = [], top= 100, order_by='oe_hdr_uid') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
        : '?';
    const select = 'order_no,oe_hdr_uid,customer_id,order_date,ship2_name,ship2_add1,' +
        'ship2_city,ship2_state,ship2_zip,ship2_country,' +
        'requested_date,terms,date_last_modified,company_id,' +
        'delete_flag,ship_to_phone,date_created,web_reference_no,validation_status,ship_confirmed_flag,cancel_flag,' +
        'validation_status,validated_via_open_orders_flag,date_order_completed,ship_confirmed_flag,warranty_return_order_flag,' +
        'edi_manual_override_flag,completed,date_order_completed,packing_basis,delivery_instructions';
    const endPoint = `${ORDER_HEADER}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

