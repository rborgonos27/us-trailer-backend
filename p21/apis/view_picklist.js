const { options, requestService, getQueryString } = require('../helper');
const { PICKLIST } = require('../enums').ERP_DATA_API;

/***
 *
 * @param token
 * @param filters
 * @param top
 * @param order_by
 * @returns {Promise<*>}
 */
module.exports = async (token, filters, top = 100, order_by = 'pick_ticket_no') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&`
        : '?';
    const select = 'company_id,pick_ticket_no,order_no,print_date,carrier_id,tracking_no' +
        ',instructions,freight_out,freight_in,ship_date,invoice_no,pick_and_hold' +
        ',date_created,date_last_modified,printed_flag,location_id,delete_flag' +
        ',direct_shipment,front_counter_tax,total_tax,front_counter' +
        ',invoice_id_when_shipped,auxiliary,freight_code_uid,invoice_batch_uid' +
        ',delivery_list_status,retrieved_by_wms,line_item_sort_sequence' +
        ',confirmable_row_status_flag,oe_pick_ticket_type_cd';
    const endPoint = `${PICKLIST}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

