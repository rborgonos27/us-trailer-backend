const { options, requestService, getQueryString } = require('../helper');
const { PICKLIST_DETAILS } = require('../enums').ERP_DATA_API;

module.exports =  async (token, filters, top = 100, order_by = 'pick_ticket_no') => {
    const credentials = {
        Authorization: `Bearer ${token}`,
    };
    const queryString = filters.length > 0 ? `${getQueryString(filters, 'and', '$filter')}&` : '?';
    const select = 'pick_ticket_no,line_number,print_quantity,ship_quantity,freight_in,unit_of_measure,unit_size,' +
        'unit_quantity,oe_line_no,qty_requested,qty_to_pick,ship_quantity,release_no,inv_mast_uid,invoice_line_uid,' +
        'date_last_modified';
    const endPoint = `${PICKLIST_DETAILS}${queryString}$format=json&$top=${top}&$orderby=${order_by}&$select=${select}`;
    const config = options(endPoint, credentials, 'GET');
    const result = await requestService(config, 'json');
    return result.value;
};

