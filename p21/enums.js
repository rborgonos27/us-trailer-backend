/***
 * API path constants
 * @type {{INVENTORY_PARTS: string, V2_PARTS: string, SERVICES: string, ENTITY_CUSTOMER: string, ENTITY_CONTACTS: string, ACCOUNTING_GL: string, VENDORS: string, ACCOUNTING_EXCHANGE_RATES: string, INVENTORY_SERIAL_NO_EXTD_INFO: string, INVENTORY_PART_SCAN: string, SALES_CONSIGNMENT_USAGE_ORDERS: string, SALES_OPPORTUNITIES: string, INVENTORY_ADJUSTMENTS: string, CONFIG: string, PURCHASE_ORDER: string, SALES_ORDERS: string, AUTH: string, SALES_TASKS: string}}
 */
const API = {
  SERVICES: '/uiserver0/api/v2/services',
  AUTH: '/api/security/token/',
  CONFIG: '/api/.configuration/',
  ENTITY_CONTACTS: '/api/entity/contacts/',
  ENTITY_ADDRESS: '/api/entity/addresses/',
  ENTITY_ORDER: '/api/entity/sales/',
  ACCOUNTING_EXCHANGE_RATES: '/api/accounting/exchangerates/',
  ACCOUNTING_GL: '/api/accounting/gl/',
  ENTITY_CUSTOMER: '/api/entity/customers/',
  VENDORS: '/api/entity/vendors/',
  INVENTORY_PARTS: '/api/inventory/parts/',
  INVENTORY_ADJUSTMENTS: '/api/inventory/inventoryadjustments/',
  V2_PARTS: '/api/inventory/v2/parts/',
  INVENTORY_SERIAL_NO_EXTD_INFO: '/api/inventory/serialnumberextdinfo/',
  INVENTORY_PART_SCAN: '/api/inventory/partscan/',
  PURCHASE_ORDER: '/api/purchasing/purchaseorders/',
  SALES_CONSIGNMENT_USAGE_ORDERS: '/api/sales/consignmentusageorders/',
  SALES_OPPORTUNITIES: '/api/sales/opportunities/',
  SALES_ORDERS: '/api/sales/orders/',
  SALES_TASKS: '/api/sales/tasks/',
};

/***
 * ERP_DATA_API path constants
 * @type {{CATEGORY: string, ITEM_CATEGORY: string, ITEM_CATEGORY_HIERARCHY: string}}
 */
const VIEW_DATA_API = {
  CATEGORY: '/data/erp/views/v1/p21_view_item_category',
  ITEM_CATEGORY: '/data/erp/views/v1/p21_view_item_category_x_inv_mast',
  INV_MASTER: '/data/erp/views/v1/p21_view_inv_mast',
  ITEM_CATEGORY_HIERARCHY: '/data/erp/views/v1/p21_view_item_category_hierarchy',
  CUSTOMER: '/data/erp/views/v1/p21_view_customer',
  CONTACTS: '/data/erp/views/v1/p21_view_contacts',
  CONTACTS_LINKS: '/data/erp/views/v1/p21_view_contacts_x_links',
  ADDRESS: '/data/erp/views/v1/p21_view_address',
  INVENTORY_LOCATION: '/data/erp/views/v1/p21_view_inv_loc',
  ORDER_HEADER: '/data/erp/views/v1/p21_view_oe_hdr',
  ORDER_DETAILS: '/data/erp/views/v1/p21_view_oe_line',
  LOCATION: '/data/erp/views/v1/p21_view_location',
  PICKLIST: '/data/erp/views/v1/p21_view_oe_pick_ticket',
  PICKLIST_DETAILS: '/data/erp/views/v1/p21_view_oe_pick_ticket_detail',
};

/***
 * PART constants
 * @type {{PART_PRICE: string, PART_AVAILABILITY: string, PART_SUBSTITUTES: string, PART_ACCESSORIES: string}}
 */
const PART = {
  PART_AVAILABILITY: 'availability',
  PART_ACCESSORIES: 'accessories',
  PART_PRICE: 'price',
  PART_SUBSTITUTES: 'substitutes'
};

/***
 *
 * @type {{EXTENDED_PROPERTIES: string, LOGICAL: string, COMPARISON: string, BASIC: string}}
 */
const OPERATION_TYPE = {
  BASIC: 'BASIC',
  LOGICAL: 'LOGICAL',
  COMPARISON: 'COMPARISON',
  EXTENDED_PROPERTIES: 'EXTENDED_PROPERTIES',
};

/***
 * P21 query logic constants
 * @type {{NOT: {type: string, value: string}, OR: {type: string, value: string}, EXTENDED_PROPERTIES: {type: string, value: string}, STARTS_WITH: {type: string, value: string}, EQUAL: {type: string, value: string}, GREATER_THAN: {type: string, value: string}, AND: {type: string, value: string}, ENDS_WITH: {type: string, value: string}, NOT_EQUAL: {type: string, value: string}, LESS_THAN: {type: string, value: string}, CONTAINS: {type: string, value: string}}}
 */
const OPERATOR = {
  EQUAL: {
    value: 'eq',
    type: OPERATION_TYPE.BASIC
  },
  NOT_EQUAL: {
    value: 'ne',
    type: OPERATION_TYPE.BASIC
  },
  GREATER_THAN: {
    value: 'gt',
    type: OPERATION_TYPE.BASIC
  },
  LESS_THAN: {
    value: 'lt',
    type: OPERATION_TYPE.BASIC
  },
  GREATER_THAN_EQUAL: {
    value: 'ge',
    type: OPERATION_TYPE.BASIC
  },
  LESS_THAN_EQUAL: {
    value: 'le',
    type: OPERATION_TYPE.BASIC
  },
  AND: {
    value: 'and',
    type: OPERATION_TYPE.LOGICAL
  },
  OR: {
    value: 'or',
    type: OPERATION_TYPE.LOGICAL
  },
  NOT: {
    value: 'not',
    type: OPERATION_TYPE.LOGICAL
  },
  STARTS_WITH: {
    value: 'startsWith',
    type: OPERATION_TYPE.COMPARISON
  },
  ENDS_WITH: {
    value: 'endWith',
    type: OPERATION_TYPE.COMPARISON
  },
  CONTAINS: {
    value: 'substringof',
    type: OPERATION_TYPE.COMPARISON
  },
  EXTENDED_PROPERTIES: {
    value: 'extendedproperties',
    type: OPERATION_TYPE.EXTENDED_PROPERTIES
  },
};

module.exports = {
  API,
  PART,
  OPERATOR,
  OPERATION_TYPE,
  ERP_DATA_API: VIEW_DATA_API,
};


