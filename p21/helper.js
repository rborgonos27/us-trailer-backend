const http = require('https');
const xmlParser = require('xml2json');
const { OPERATION_TYPE } = require('./enums');
require('dotenv').config();

/**
 *
 * @param config
 * @param format
 * @returns {Promise<unknown>}
 */
const requestService = (config, format='json') => {
    return new Promise(function (resolve, reject) {
        const request = prepareRequest(config, format, resolve);

        request.on('error', function (error) {
            reject(error);
        });

        request.end();
    });
};

/**
 *
 * @param config
 * @param format
 * @returns {Promise<unknown>}
 */
const writeRequestService = (config, format='json') => {
    return new Promise(function (resolve, reject) {
        const request = prepareRequest(config, format, resolve);

        request.on('error', function (error) {
            console.log(error);
            reject(error);
        });

        request.write(JSON.stringify(config.body));

        request.end();
    });
};

const prepareRequest = (config, format, resolve) => {
    return http.request(config, function (res) {
        let data = '';
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            const response = format === 'xml' ? xmlParser.toJson(data) : data;
            resolve(JSON.parse(response));
        });
    });
};

/**
 *
 * @param path
 * @param headers
 * @param method
 * @returns {{path: *, headers: *, method: string, port: string, host: string}}
 */
const options = (path, headers, method = 'GET') => {
    headers['Content-Length'] = 0;
    const host = process.env.P21_HOST;
    const port = process.env.P21_PORT;
    if (process.env.REST_DEBUG === '1') {
        console.log('REST DEBUG: ', host, port, path, method, headers);
    }
    return {
        host,
        port,
        path,
        method,
        headers
    }
};

/***
 *
 * @param filter
 * @returns {string}
 */
const getQueryStringPart = (filter) => {
    const {type, field, value} = filter;
    if (type.type === OPERATION_TYPE.COMPARISON) {
        return `${type.value}('${value}', ${field})`;
    } else if (type.type === OPERATION_TYPE.BASIC) {
        if (!value && !Number.isInteger(value)) return `${field} ${type.value} ${null}`;
        const filterValue = (Number.isInteger(value) || value.startsWith('datetime')) ? value : `'${value}'`;
        return `${field} ${type.value} ${filterValue}`.replace('&','%26');
    } else if (type.type === OPERATION_TYPE.EXTENDED_PROPERTIES) {
        return `${type.value}=${value}`;
    }
};

/***
 *
 * @param filters
 * @param operator
 * @param syntax
 * @returns {string}
 */
const getQueryString = (filters, operator = 'and', syntax='$query') => {
    if (filters.length === 0) return '';
    const queryStringArray = [];
    const queryArray = [];
    filters.map((filter) => {
        const { type } = filter;
        if (type.type === OPERATION_TYPE.EXTENDED_PROPERTIES) {
            queryStringArray.push(getQueryStringPart(filter));
        } else {
            queryArray.push(getQueryStringPart(filter));
        }
    });
    const queryFilter = queryArray.join(` ${operator} `);
    queryStringArray.push(`${syntax}=${queryFilter}`);
    return encodeURI(`?${queryStringArray.join('&')}`).replace('+', '%2B');
};

/***
 *
 * @param path
 * @param headers
 * @param method
 * @param body
 * @returns {{path: *, headers: *, method: string, port: string, host: string, body: *}}
 */
const writeOption = (path, headers, method='POST', body = null) => {
    headers['Content-Length'] = body ? JSON.stringify(body).length : 0;
    const isTestWrite = process.env.TEST_WRITE_P21;
    const host = isTestWrite === '1' ? process.env.TEST_P21_HOST : process.env.P21_HOST;
    const port = isTestWrite === '1' ? process.env.TEST_P21_PORT : process.env.P21_PORT;
    if (process.env.REST_DEBUG === '1') {
        console.log('REST DEBUG: ', host, port, path, method, headers);
    }
    return {
        host,
        port,
        path,
        method,
        headers,
        body,
    }
};

module.exports = { requestService, writeRequestService, options, writeOption, getQueryString };
