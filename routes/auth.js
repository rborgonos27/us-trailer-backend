const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../big_commerce/apis/user');
require('dotenv').config();

router.post('/login', (req, res) => {
    const params = req.body;
    if (Object.keys(params).length !== 2 || !params.username || !params.password) {
        res.status(400);
        res.send({
            message: 'Username and password are required'
        });
    }
    if (params.username === process.env.UST_USERNAME) {
        if(bcrypt.compareSync(params.password, process.env.UST_PASSWORD)) {
            const payload = {
                username: params.username
            };
            const token = jwt.sign(payload, process.env.JWT_SECRET, {
                expiresIn: '30d',
            });
            payload.token = token;
            payload.expiresIn = '30d';
            res.send(payload);
        } else {
            res.status(400);
            res.send({
                message: 'Username and password not matched'
            });
        }
    }
});

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

router.get('/reset-password', async (req, res) => {
    new Promise(async (resolve, reject) =>{
        let email = req.query.email.replace(' ', '+');
        await sleep(1000);
        const forgotPassword = await User.forgotPassword(email);
        console.log(forgotPassword.status);
        const path = require('path');
        console.log(path);
        return res.render(path.join(__dirname+'/resetPassword.html'));
    }).catch(err=> res.json(err));
});

module.exports = router;