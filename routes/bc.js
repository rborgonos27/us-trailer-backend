const express = require('express');
const router = express.Router();
const Wishlists = require('../big_commerce/apis/wishlist');
const Products = require('../big_commerce/apis/product');

const convertObjectToQueryString = (object) => {
    const str = [];
    for (let p in object)
        if (object.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(object[p]));
        }
    return str.join("&");
};

router.get('/wishlists', async (req, res) => {
    const queryString = Object.keys(req.query).length > 0 ? `${convertObjectToQueryString(req.query)}` : '';
    const wishList = await Wishlists.getWishLists(queryString);
    return res.send(wishList);
});

router.get('/products/:id', async (req, res) => {
    let id = req.params.id;
    const product = await Products.getProduct(id);
    return res.send(product);
});

router.get('/products', async (req, res) => {
    const queryString = Object.keys(req.query).length > 0 ? `${convertObjectToQueryString(req.query)}` : '';
    const products = await Products.getProducts(queryString);
    return res.send(products);
});

router.get('/products/:id/custom-fields', async (req, res) => {
    let id = req.params.id;
    const productCustomFields = await Products.customFields.getCustomFields(id);
    return res.send(productCustomFields);
});

module.exports = router;