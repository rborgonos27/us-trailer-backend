require('./p21');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const Airbrake = require('@airbrake/node');
const airbrakeExpress = require('@airbrake/node/dist/instrumentation/express');

const app = express();
const bcRoute = require('./routes/bc');
const authRoute = require('./routes/auth');

const userAuth = require('./utils/auth');

const airbrake = new Airbrake.Notifier({
    projectId: process.env.AIRBRAKE_PROJECT_ID,
    projectKey: process.env.AIRBRAKE_PROJECT_KEY,
});
app.use(airbrakeExpress.makeMiddleware(airbrake));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT || 5000;

// run cron
require('./cron');

app.use('/auth', authRoute);
app.get('/', (req, res) => {
    return res.send({ 'name': 'US Trailer Backend Integration', 'version': '1' });
});
app.use(userAuth);
app.use('/bigcommerce', bcRoute);

// when there's a post request to /webooks...
app.post('/hooks', function (req, res) {
    require('./webhooks').processResponse(req.body);
    res.send('OK');
});

require('./webhooks').initialize();
app.listen(port, () => console.log(`Listening to port ${port}.`));
