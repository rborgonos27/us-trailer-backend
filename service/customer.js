const Customer = require('../big_commerce/apis/customer');
const Address = require('../big_commerce/apis/address');
const { OPERATOR } = require('../p21/enums');
const { matchedHelper } = require('../cron/helper');

const ViewCustomer = require('../p21/apis/view_customer');
const { getToken } = require('../utils/p21');
const { getDataFromArray, checkValidArray } = require('../utils/helper');
const P21Customer = require('../p21/apis/customer');
const P21Contact = require('../p21/apis/contacts');
const P21Address = require('../p21/apis/address');

const ViewAddress = require('../p21/apis/view_address');
const ViewContacts = require('../p21/apis/view_contacts');
const {
    SALES_REP_ID,
    CUSTOMER_ID,
    PRICING_METHOD,
    DEFAULT_CONTACT_TITLE,
    COD_REQUIRED_FLAG,
    WEB_ENABLED_FLAG,
} = require('../utils/constants').P21;


const validateDetails = async (id) => {
    let customer = null;
    let address = null;
    let p21Customer = null;
    let p21Address = null;
    const token = await getToken();
    try {
        const customerResult = await Customer.getCustomers(`id:in=${id}`);
        console.log(customerResult);
        customer = getDataFromArray(customerResult.data);
        if (!customer) customer = null;

        // get customer address
        const addressResult = await Address.getAddresses(`customer_id:in=${id}`);
        console.log(customerResult);
        address = getDataFromArray(addressResult.data);
        if (!address) address = null;

        let filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'customer_name',
                value: customer.company,
            },
        ];
        // p21 validation
        p21Customer = await ViewCustomer(token, filters);

        filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'email_address',
                value: customer.email,
            },
        ];
        console.log(filters, 'filters');
        p21Address = await ViewAddress(token, filters);
        console.log(p21Address);
    } catch (error) {
        console.log(error);
        return null;
    }
    return {
        customer,
        address,
        p21Customer,
        p21Address,
    };
};

const updateCustomer = async (id, skipServerUpdate = false) => {
    const token = await getToken();
    try {
        const {
            address,
            customer,
            p21Customer,
            p21Address,
        } = await validateDetails(id);
        console.log('update customer', p21Address);
        if (p21Address.length > 0) {
            // check if contact exists
            const contactsFilters = [
                {
                    type: OPERATOR.EQUAL,
                    field: 'first_name',
                    value: customer.first_name,
                },
                {
                    type: OPERATOR.EQUAL,
                    field: 'last_name',
                    value: customer.last_name,
                }
            ];
            // if exists
            const p21Contacts = await ViewContacts(token, contactsFilters);
            if (Array.isArray(p21Contacts) && p21Contacts.length === 0) {
                // update contacts
                const contactData = {
                    FirstName: customer.first_name,
                    Mi: '',
                    LastName: customer.last_name,
                    Title: DEFAULT_CONTACT_TITLE,
                    AddressId: p21Address[0].id,
                    DirectPhone: customer.phone,
                    PhoneExt: customer.phone,
                    DirectFax: customer.phone,
                    Cellular: customer.phone,
                    HomeAddress1: address.address1,
                    HomeAddress2: address.address2 || '',
                    HomePhone: customer.phone,
                    HomeFax: customer.phone,
                    EmailAddress: customer.email
                };
                console.log(contactData);
                if (!skipServerUpdate) {
                    return await P21Contact.saveContact(token, contactData);
                }
            }
        }
    } catch (error) {
        console.log(error);
    }
};

// get customer from big commerce
const addCustomer = async (id, skipServerUpdate = false) => {
    const token = await getToken();
    try {
        const {
            address,
            customer,
            p21Address,
        } = await validateDetails(id);
        console.log('add customer', p21Address);
        // if company exists create contacts
        if (p21Address.length === 0) {
            console.log('customer add');
            // add customer
            const addressData = {
                MailAddress1: address.address1,
                MailAddress2: address.address2 || '',
                MailCity: address.city,
                MailState: matchedHelper.matchStateByName(address.state_or_province),
                MailPostalCode: address.postal_code,
                MailCountry: address.country,
                CentralPhoneNumber: customer.phone,
                CentralFaxNumber: customer.phone,
                PhysAddress1: address.address1,
                PhysAddress2: address.address2,
                PhysCity: address.city,
                PhysState: matchedHelper.matchStateByName(address.state_or_province),
                PhysPostalCode: address.postal_code,
                EmailAddress: customer.email,
                PhysCounty: address.country,
            };

            const customerData = {
                CompanyId: `${CUSTOMER_ID}`,
                CustomerName: `${customer.company === '' ? `${customer.first_name} ${customer.last_name}` : customer.company}`,
                SalesrepId: `${SALES_REP_ID}`,
                CodRequiredFlag: COD_REQUIRED_FLAG,
                PricingMethod: PRICING_METHOD,
                WebEnabledFlag: WEB_ENABLED_FLAG,
                CustomerAddress: addressData,
                CreditLimit: 10000,
                CustomerShipTos: {
                    list: [
                        {
                            ShipToAddress: addressData
                        }
                    ]
                },
            };
            let customerDataResponse = null;
            if (!skipServerUpdate) {
                customerDataResponse = await P21Customer.saveCustomer(token, customerData);
            }
            if (customerDataResponse) {
                const contactData = {
                    FirstName: customer.first_name,
                    MI: '',
                    LastName: customer.last_name,
                    Title: DEFAULT_CONTACT_TITLE,
                    AddressId: customerDataResponse.CustomerId,
                    DirectPhone: customer.phone,
                    PhoneExt: customer.phone,
                    DirectFax: customer.phone,
                    Cellular: customer.phone,
                    HomeAddress1: address.address1,
                    HomeAddress2: address.address2 || '',
                    HomePhone: customer.phone,
                    HomeFax: customer.phone,
                    EmailAddress: customer.email
                };
                if (!skipServerUpdate) {
                    return await P21Contact.saveContact(token, contactData);
                    // console.log(p21Contact, 'test');
                }
            }
        }
    } catch (error) {
        console.log(error);
    }
};


const updateCustomerAddress = async (id) => {
    // get customer address on big commerce
    const addressResult = await Address.getAddresses(`id:in=${id}`);
    const address = getDataFromArray(addressResult.data);
    if (!address) return null;

    const token = await getToken();

    const filters = [
        {
            type: OPERATOR.EQUAL,
            field: 'customer_name',
            value: address.company,
        },
    ];
    // search address to p21
    const p21Address = await ViewAddress(token, filters);
    if (checkValidArray(p21Address)) {
        try {
            // if exist update
            const addressData = {
                MailAddress1: address.address1,
                MailAddress2: address.address2 || '',
                MailCity: address.city,
                MailState: matchedHelper(address.state_or_province),
                MailPostalCode: address.postal_code,
                MailCountry: address.country,
                CentralPhoneNumber: address.phone,
                CentralFaxNumber: address.phone,
                PhysAddress1: address.address1,
                PhysAddress2: address.address2,
                PhysCity: address.city,
                PhysState: matchedHelper.matchState(address.state_or_province),
                PhysPostalCode: address.postal_code,
                AddressId: address.address1,
                PhysCounty: address.country,
            };
            return await P21Address.updateAddress(token, p21Address[0].address_id, addressData);
            // console.log(updateAddress);
        } catch (error) {
            console.log(error);
        }
    }

};

const deleteCustomer = async (id) => {
    const customerResult = await Customer.getCustomers(`id:in=${id}`);
    const customer = getDataFromArray(customerResult.data());
    if (!customer) return null; // TODO:: handle this cleanly

    const token = await getToken();
    const filters = [
        {
            type: OPERATOR.EQUAL,
            field: 'customer_name',
            value: cusotmer.company,
        }
    ];
    const p21Customer = await ViewCustomer(token, filters);
    if (checkValidArray(p21Customer)) {
        try {
            const customerId = p21Customer[0].customer_id;
            const customerData = {
                DeleteFlag: 'Y',
                CustomerId: customerId
            };

            return await P21Customer.updateCustomer(token, customerId, customerData);
        } catch (error) {
            console.log(error);
        }
    }
};


module.exports = {
    validateDetails,
    deleteCustomer,
    updateCustomer,
    updateCustomerAddress,
    addCustomer,
};
