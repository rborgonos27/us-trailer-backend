const Order = require('../big_commerce/apis/order');
const Customer = require('../big_commerce/apis/customer');
const Address = require('../big_commerce/apis/address');
const Wishlist = require('../big_commerce/apis/wishlist');
const { OPERATOR } = require('../p21/enums');
const { getToken } = require('../utils/p21');
const { getDataFromArray } = require('../utils/helper');

const P21SalesOrder = require('../p21/apis/sales_orders');
const ViewCustomer = require('../p21/apis/view_customer');
const ViewContacts = require('../p21/apis/view_contacts');
const ViewOrderHeader = require('../p21/apis/view_order_header');
const ViewInvMaster = require('../p21/apis/view_inv_master');
const moment = require('moment');
const _ = require('lodash');
const { escapeQuote, restLog } = require('../utils/helper');

const { DATE_FORMAT } = require('../utils/constants').P21;

const {
    MANUAL_PRICE_OVERRIDE,
    UNIT_OF_MEASURE,
    CURRENCY_ID,
} = require('../utils/constants').P21;

let token = null;

/***
 *
 * @param id
 * @returns {Promise<{
 * bigCommerce: {address: null, wishlist: null, orderItems: *, order: *, customer: null},
 * p21: {p21Item: {displayDesc: *, itemCategoryUid: *, id: *, invMasterUid: *}
 * [], p21OrderHeader: *, p21Customer: null, p21Contact: null}}|null>}
 */
const validateDetails = async (id) => {
    let order = null;
    let orderItems = null;
    let customer = null;
    let address = null;
    let wishlist = null;
    let p21Customer = null;
    let p21Contact = null;
    let p21OrderHeader = null;
    let p21Item = null;

    token = await getToken();
    const orderResult = await Order.getOrders(id);
    order = orderResult;
    if (!order) return null;
    if (order && order.status === 'Incomplete') {
        return {
            bigCommerce: null,
            p21: null
        }
    }

    // get order items
    const orderItemsResult = await Order.getOrderProducts(id);
    orderItems = orderItemsResult;
    const itemQuery = orderItems.map(item => {
        return {
            type: OPERATOR.EQUAL,
            field: 'item_id',
            value: item.sku
        };
    });
    p21Item = await ViewInvMaster(token, itemQuery, 100, 'item_id', 'or');
    if (!orderItems) return null;
    const customerId = order.customer_id;
    restLog('Customer Id', customerId);
    if (customerId !== 0) {
        const customerResult = await Customer.getCustomers(`id:in=${customerId}`);
        customer = getDataFromArray(customerResult.data);
        restLog(customer, 'customer is logged in');

        // get customer wishlists
        wishlist = await Wishlist.getWishLists(`customer_id=${customerId}`);
        wishlist = wishlist.data;

        const addressResult = await Address.getAddresses(`customer_id:in=${customerId}`);
        address = getDataFromArray(addressResult.data);

        const customerName = customer.company === '' ? `${customer.first_name} ${customer.last_name}` : customer.company;
        // get customer
        const filters = [
            {
                type: OPERATOR.EQUAL,
                field: 'customer_name',
                value: escapeQuote(customerName),
            },
        ];

        p21Customer = await ViewCustomer(token, filters);
        p21Customer = getDataFromArray(p21Customer);
        // get contacts
        const contactsFilters = [
            {
                type: OPERATOR.EQUAL,
                field: 'first_name',
                value: customer.first_name.length > 15 ? customer.first_name.substr(0, 15) : customer.first_name,
            },
            {
                type: OPERATOR.EQUAL,
                field: 'last_name',
                value: customer.last_name.length > 15 ? customer.last_name.substr(0, 15) : customer.last_name,
            }
        ];
        p21Contact = await ViewContacts(token, contactsFilters);
        p21Contact = getDataFromArray(p21Contact);
    }

    const orderHeaderFilters = [
        {
            type: OPERATOR.EQUAL,
            field: 'web_reference_no',
            value: `${order.id}`
        }
    ];

    p21OrderHeader = await ViewOrderHeader(token, orderHeaderFilters);

    const bigCommerce  = {
        order,
        orderItems,
        customer,
        address,
        wishlist,
    };

    const p21 = {
        p21Customer,
        p21Contact,
        p21OrderHeader,
        p21Item,
    };

    return {
        bigCommerce,
        p21,
    };
};

/***
 *
 * @param p21Item
 * @param sku
 * @returns {string}
 */
const getP21ItemUnit = (p21Item, sku) => {
    const matchedItem = _.filter(p21Item, (item) => {
       return sku === item.item_id;
    });
    return Array.isArray(matchedItem) && matchedItem.length > 0 ? matchedItem[0].default_selling_unit : UNIT_OF_MEASURE;
};

/***
 *
 * @param wishlist
 * @param product
 * @returns {*[]|*}
 */
const getJobListNotes = (wishlist, product) => {
    if (!wishlist) return [];
    return wishlist.map(list => {
       const matched = _.filter(list.items, item => {
           return item.product_id === product.product_id;
       });
       if (matched.length > 0) {
           let topic = `JL #${list.id}: ${list.name}`;
           if (topic.length > 30) {
               topic = topic.substr(0, 30);
           }
           return {
               Topic: topic,
               Note: `SKU: ${product.sku}`
           };
       } else return {
           Topic: 'Reg Item',
           Note: ``
       };
    });
};

/***
 *
 * @param orderItems
 * @param p21Item
 * @param wishlist
 * @returns {*}
 */
const getProductLineParams = (orderItems, p21Item, wishlist) => {
    let lineNo = 0;
    return orderItems.map(item => {
        return {
            LineNo: ++lineNo,
            ItemId: item.sku,
            UnitQuantity: item.quantity,
            UnitOfMeasure: getP21ItemUnit(p21Item, item.sku),
            UnitPrice: item.base_price,
            ManualPriceOveride: MANUAL_PRICE_OVERRIDE,
            Notes: {
                list: getJobListNotes(wishlist, item)
            }
        };
    });
};

/***
 *
 * @param bigCommerce
 * @param p21
 * @returns {{}}
 */
const createOrderParameter = (bigCommerce, p21) => {
    const parameter = {};
    const { order, orderItems, wishlist } = bigCommerce;
    const { p21Customer, p21Contact, p21Item } = p21;

    const orderDate = moment(order.date_created).format(DATE_FORMAT);
    parameter.CustomerId = p21Customer.customer_id;
    parameter.CompanyId = p21Customer.company_id;
    parameter.ContactId = p21Contact.id;
    parameter.OrderDate = orderDate;
    parameter.Approved = true; // auto approved
    parameter.CurrencyID = CURRENCY_ID;
    parameter.WebReferenceNo = order.id;
    parameter.PoNo = `Online Order: ${order.id}`;
    parameter.FreightOut = order.base_shipping_cost ? order.base_shipping_cost : 0;
    parameter.Lines = {
        list: getProductLineParams(orderItems, p21Item, wishlist)
    };

    return parameter;
};

/**
 *
 * @param id
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const createOrder = async (id, skipServerUpdate = false) => {
    try {
        let { bigCommerce, p21 } = await validateDetails(id);
        if (!bigCommerce || !p21) {
            throw new Error('Invalid big commerce or p21 order');
        }
        token = await getToken();

        let { p21Customer, p21Contact, p21OrderHeader } = p21;
        restLog(p21Customer, p21Contact);
        let params = null;
        let p21Order = null;

        if (p21Customer && p21Contact) {
            params = createOrderParameter(bigCommerce, p21);
            restLog(params);
            restLog(params.Lines);
            if (!skipServerUpdate) {
                if (!p21OrderHeader || p21OrderHeader.length === 0) {
                    p21Order = await P21SalesOrder.saveOrder(token, params);
                } else {
                    p21Order = p21OrderHeader.length > 0 ? p21OrderHeader[0] : null;
                }
                if (p21Order) {
                    const orderNo = p21Order.OrderNo ? p21Order.OrderNo : p21Order.order_no;
                    const body = {
                        staff_notes: `Prophet21 Order No: ${orderNo}`,
                        customer_message: bigCommerce.order.customer_message.replace('force sync', 'order synced successfully.')
                    };
                    const bcOrder = await Order.updateOrder(id, body);
                    restLog('Successfully updated order: ', bcOrder);
                    restLog('Successfully synced order: ', p21Order);
                }
            }
            return p21Order;
        }
    } catch (error) {
        restLog('Create Order Error: ', error);
    }
};

const updateOrder = async (id) => {
    try {
        let { bigCommerce, p21 } = await validateDetails(id);
        if (!bigCommerce || !p21) {
            throw new Error('Invalid big commerce or p21 order');
        }
        const { customer_message } = bigCommerce.order;
        restLog(customer_message);
        if (customer_message.search('force sync') > -1) {
            restLog('message', customer_message);
            await createOrder(id);
        }
    } catch (error) {
        restLog('Update Order Error: ', error);
    }
};

module.exports = {
    createOrder,
    updateOrder,
    validateDetails,
    createOrderParameter,
};
