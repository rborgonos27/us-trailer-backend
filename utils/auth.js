const jwt = require('jsonwebtoken');

/***
 *
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
module.exports = (req, res, next) => {
    let redirectUrl = req.originalUrl;
    const anonymousURL = [ '/', '/favicon.ico', '/auth', '/auth/login', '/auth/reset-password'];
    let token = '';
    if (redirectUrl === '/favicon.ico') {
        return next();
    } else if(anonymousURL.includes(redirectUrl)) {
        return next();
    } else {
        if (req.body && req.body.token) {
            token = req.body.token;
        } else if (req.query.token) {
            token = req.query.token;
        } else if (req.headers['access-token']) {
            token = req.headers['access-token'];
        }
    }
    if (token) {
        new Promise((resolve, reject) => {
            return jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
                if (err) {
                    reject('Failed to authenticate user token.');
                } else {
                    resolve(decoded);
                }
            });
        }).then(async data =>{
            req.decoded = data;
            try {
                if (data.username === process.env.UST_USERNAME) {
                    // console.log('success.');
                    next();
                } else {
                    return res.status(403).send({
                        success: false,
                        message: 'Unauthorized.'
                    });
                }
            } catch (error) {
                return res.status(403).send({
                    success: false,
                    message: error.message
                });
            }
        }).catch(err =>{
            return res.json({ success: false, message: err })
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};
