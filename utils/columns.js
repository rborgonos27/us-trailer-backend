require('dotenv').config();
const _ = require('lodash');

/***
 *
 * @type {{GenerateSchema: (function(*, *=, *=): *)}}
 */
const Columns = {
    GenerateSchema: (cols, valSuffix = false, exclude = []) => {
        const schema = {};
        for(const key in cols){
            schema[key] = cols[key].type;
            if (valSuffix) {
                schema[`${key}_val`] = cols[key].type;
            }
        }
        return _.omit(schema, exclude);
    },
};

module.exports = Columns;
