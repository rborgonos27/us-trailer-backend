/**
 *
 * @type {{CURRENCY_ID: string,
 * DEFAULT_CONTACT_TITLE: string, DATE_FORMAT: string,
 * CUSTOMER_ID: string, SALES_REP_ID: string,
 * PRICING_METHOD: string, COD_REQUIRED_FLAG: string,
 * UNIT_OF_MEASURE: string, MANUAL_PRICE_OVERRIDE: string, WEB_ENABLED_FLAG: string}}
 */
const P21 = {
    SALES_REP_ID: '5961',
    CUSTOMER_ID: 'USTP',
    PRICING_METHOD: 'Manual',
    UNIT_OF_MEASURE: 'EA',
    MANUAL_PRICE_OVERRIDE: 'Y',
    CURRENCY_ID: 'EA',
    DEFAULT_CONTACT_TITLE: 'SHOP CONTACT',
    COD_REQUIRED_FLAG: 'N',
    WEB_ENABLED_FLAG: 'Y',
    DATE_FORMAT: 'YYYY-MM-DDTHH:mm:ss.SSS'
};

/**
 * BigCommerce Constants
 * @type {
 * { PRODUCT_TYPE: string, PRODUCT_AVAILABILITY: string, CATEGORY_DOORS: string, QUERY_DATE_FORMAT: string,
 * CATEGORY_TRAILER: string, ADDRESS_TYPE: string, CATEGORY_SHOP_SUPPLIES: string, P21_SEPARATOR: string,
 * DEFAULT_COUNTRY_CODE: string, SHOP_ALL: string, BC_SEPARATOR: string}}
 */
const BigCommerce = {
    PRODUCT_TYPE: 'physical',
    PRODUCT_AVAILABILITY: 'available',
    DEFAULT_COUNTRY_CODE: 'US',
    ADDRESS_TYPE: 'commercial',
    SHOP_ALL: 'Shop All',
    CATEGORY_TRAILER: 'trailer',
    CATEGORY_TRAILER_LABEL: 'trailer body',
    CATEGORY_DOORS: 'doors',
    CATEGORY_SHOP_SUPPLIES: 'shop supplies',
    BC_SEPARATOR: 'and',
    P21_SEPARATOR: '&',
    QUERY_DATE_FORMAT: 'YYYY-MM-DD',
};

/**
 * General Constants
 * @type {{MOMENT_LAST_HOUR: number}}
 */
const General = {
    MOMENT_LAST_HOUR: 54,
    MOMENT_LAST_MINUTE: 60,
};

/**
 *
 * @type {{REFUNDED: number, INCOMPLETE: number, AWAITING_PAYMENT: number,
 * DISPUTED: number, MANUAL_VERIFICATION_REQUIRED: number, PENDING: number,
 * AWAITING_PICKUP: number, DECLINED: number, CANCELLED: number,
 * AWAITING_SHIPMENT: number, COMPLETED: number, PARTIALLY_SHIPPED: number,
 * AWAITING_FULFILLMENT: number, PARTIALLY_REFUNDED: number, SHIPPED: number}}
 */
const BigCommerceOrderStatus = {
    INCOMPLETE: 0,
    PENDING: 1,
    SHIPPED: 2,
    PARTIALLY_SHIPPED: 3,
    REFUNDED: 4,
    CANCELLED: 5,
    DECLINED: 6,
    AWAITING_PAYMENT: 7,
    AWAITING_PICKUP: 8,
    AWAITING_SHIPMENT: 9,
    COMPLETED: 10,
    AWAITING_FULFILLMENT: 11,
    MANUAL_VERIFICATION_REQUIRED: 12,
    DISPUTED: 13,
    PARTIALLY_REFUNDED: 14
};

/**
 *
 * @type {{US_TRAILER_DELIVERY: string, FREE_SHIPPING: string, PICKUP_IN_CHICAGO: string, PICKUP_IN_MILWAUKEE: string}}
 */
const BigCommerceShippingMethod = {
    PICKUP_IN_CHICAGO: 'Pickup In Store - Chicago Warehouse',
    PICKUP_IN_MILWAUKEE: 'Pickup In Store - Milwaukee Warehouse',
    US_TRAILER_DELIVERY: 'Shipping (For orders over $150+)',
    FREE_SHIPPING: 'Free Shipping',
};


/**
 * pageValues constants
 * @type {{DEFAULT_PAGE: number, DEFAULT_LIMIT: number, DEFAULT_SLEEP: number}}
 */
const pageValues = {
    DEFAULT_LIMIT: 250,
    DEFAULT_PAGE: 1,
    DEFAULT_SLEEP: 300,
};

module.exports = {
    P21,
    BigCommerce,
    pageValues,
    General,
    BigCommerceOrderStatus,
    BigCommerceShippingMethod,
};
