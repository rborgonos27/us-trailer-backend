const crypto = require('crypto');

const ALGORITHM = 'aes256';
const KEY = 'yekterces';
const UTF8_ENCODING = 'utf8';
const HEX_ENCODING = 'hex';

/**
 *
 * @param textToEncrypt
 * @returns {string}
 */
const encrypt = (textToEncrypt) => {
    const cipher = crypto.createCipher(ALGORITHM, KEY);
    const encrypted = cipher.update(textToEncrypt, UTF8_ENCODING, HEX_ENCODING);
    return encrypted + cipher.final(HEX_ENCODING);
};

/**
 *
 * @param encryptedText
 * @returns {string}
 */
const decrypt = (encryptedText) => {
    const decipher = crypto.createDecipher(ALGORITHM, KEY);
    const decrypted = decipher.update(encryptedText, HEX_ENCODING, UTF8_ENCODING);
    return decrypted + decipher.final(UTF8_ENCODING);
};

module.exports = {
    encrypt,
    decrypt,
};
