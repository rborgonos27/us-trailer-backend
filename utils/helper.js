/***
 *
 * @param url
 * @returns {string}
 */
const extractHostname = (url) => {
    let hostname;

    if (url.indexOf('//') > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    hostname = hostname.split(':')[0];

    hostname = hostname.split('?')[0];

    return hostname;
};

/***
 *
 * @param filters
 * @returns {string}
 */
const buildQueryString = (filters) => {
    if (filters.length === 0) return '';
    const queryStringArray = [];
    filters.map((filter) => {
        const { field, value } = filter;
        queryStringArray.push(`${field}=${value}`);
    });
    return queryStringArray.join('&');
};

/***
 *
 * @param ms
 * @returns {Promise<unknown>}
 */
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

/***
 *
 * @param data
 * @returns {arg is any[]}
 */
const checkArray = (data) => {
    return Array.isArray(data);
};

/***
 *
 * @param data
 * @returns {*}
 */
const getDataFromArray = (data) => {
    return checkArray(data) && data.length > 0 ? data[0] : null;
};

/***
 *
 * @param data
 * @returns {boolean}
 */
const checkValidArray = (data) => {
    return checkArray(data) && data.length > 0;
};

/***
 *
 * @param data
 * @returns {boolean}
 */
const checkInvalidOrEmptyArray = (data) => {
    return !checkArray(data) || data.length === 0;
};

/***
 *
 * @param input
 * @returns {boolean}
 */
const checkInputString = (input) => {
     return input !== '';
};

/***
 *
 * @param label
 * @param value
 * @returns {boolean}
 */
const restLog = (label, value = '') => {
   if (process.env.REST_DEBUG === '1') {
       console.log(label, value);
   } else {
       return false;
   }
   return true;
};

/***
 *
 * @param label
 * @returns {boolean}
 */
const restLogTime = (label, isEnd = false) => {
    if (process.env.REST_DEBUG === '1') {
        if (!isEnd) {
            console.time(label);
        } else {
            console.timeEnd(label);
        }
    } else {
        return false;
    }
    return true;
};

/***
 *
 * @param str
 * @returns {*}
 */
const escapeQuote = (str) => {
    return str.replace(`'`, `''`);
};

module.exports = {
    extractHostname,
    buildQueryString,
    sleep,
    getDataFromArray,
    checkValidArray,
    checkInvalidOrEmptyArray,
    checkArray,
    checkInputString,
    restLog,
    restLogTime,
    escapeQuote,
};
