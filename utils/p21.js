const Auth = require('../p21/apis/auth');

/***
 *
 * @returns {Promise<*>}
 */
const getToken = async () => {
    try {
        const username = process.env.P21_USERNAME;
        const password = process.env.P21_PASSWORD;
        const authResponse = await Auth(username, password);
        return authResponse.AccessToken;
    } catch (error) {
        console.log('Error getting token', error);
    }
};

module.exports = {
    getToken
};
