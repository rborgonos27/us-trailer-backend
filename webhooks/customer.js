const { addCustomer, updateCustomer, updateCustomerAddress, deleteCustomer } = require('../service/customer');
const { hookScope } = require('./enums');

/***
 *
 * @param id
 * @param scope
 * @param skipServerUpdate
 * @returns {Promise<void>}
 */
const hookCondition = async (id, scope, skipServerUpdate = false) => {
    const { CREATE_CUSTOMER, UPDATE_CUSTOMER, UPDATE_CUSTOMER_ADDRESS, DELETE_CUSTOMER } = hookScope;
    switch (scope) {
        case CREATE_CUSTOMER:
            if (!skipServerUpdate) {
                await addCustomer(id);
            }
            break;
        case UPDATE_CUSTOMER:
            if (!skipServerUpdate) {
                await updateCustomer(id);
            }
            break;
        case UPDATE_CUSTOMER_ADDRESS:
            if (!skipServerUpdate) {
                await updateCustomerAddress(id);
            }
            break;
        case DELETE_CUSTOMER:
            if (!skipServerUpdate) {
                await deleteCustomer(id);
            }
            break;
        default:
            break;
    }
};

module.exports = {
    hookCondition
};
