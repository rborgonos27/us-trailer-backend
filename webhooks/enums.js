/***
 *
 * @type {{
 * UPDATE_CUSTOMER: string,
 * DELETE_CUSTOMER_ADDRESS: string,
 * CREATE_CUSTOMER: string,
 * UPDATE_CUSTOMER_ADDRESS: string,
 * UPDATE_ORDER: string,
 * DELETE_CUSTOMER: string,
 * CREATE_ORDER: string,
 * ARCHIVED_ORDER: string,
 * UPDATE_STATUS_ORDER: string,
 * CREATE_CUSTOMER_ADDRESS: string}}
 */
const hookScope = {
   CREATE_CUSTOMER: 'store/customer/created',
   UPDATE_CUSTOMER: 'store/customer/updated',
   DELETE_CUSTOMER: 'store/customer/deleted',
   CREATE_CUSTOMER_ADDRESS: 'store/customer/address/created',
   UPDATE_CUSTOMER_ADDRESS: 'store/customer/address/updated',
   DELETE_CUSTOMER_ADDRESS: 'store/customer/address/deleted',
   CREATE_ORDER: 'store/order/created',
   UPDATE_ORDER: 'store/order/updated',
   ARCHIVED_ORDER: 'store/order/archived',
   UPDATE_STATUS_ORDER: 'store/order/statusUpdated'
};

const hookName = {
   PRODUCT: 'product',
   ORDER: 'order',
   CUSTOMER: 'customer'
};


module.exports = {
   hookScope,
   hookName
};
