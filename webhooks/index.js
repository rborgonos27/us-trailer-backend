const bigCommerceWebhooks = require('../big_commerce/apis/webhooks');
const _ = require('lodash');
const { sleep, restLog } = require('../utils/helper');

const { hookCondition } = require('./customer');
const { hookOrderCondition } = require('./order');

const { hookScope, hookName } = require('./enums');

/**
 *
 * @param webhooks
 * @param scope
 * @returns {Array}
 */
const matchHookScope = (webhooks, scope) => {
    return _.filter(webhooks, (hook) => {
        return hook.scope === scope;
    });
};

/**
 *
 * @param skipUpdateServer
 * @returns {Promise<void>}
 */
const initialize = async (skipUpdateServer = false) => {
    const webHooks = await bigCommerceWebhooks.getWebhooks();
    const promises = [];
    for (const key in hookScope) {
        const scope = hookScope[key];
        const matchHook = matchHookScope(webHooks, scope);
        if (matchHook.length === 0) {
            const destination = process.env.WEBHOOK_ENDPOINT;
            const hookRequestBody = {
                scope,
                destination,
                is_active: true,
                headers: {
                    'access-token': process.env.WEBHOOK_TOKEN,
                }
            };
            if (!skipUpdateServer) {
                promises.push(await addWebHook(hookRequestBody));
            }
        }
    }
    Promise.all(promises).then(() => {
        console.log('All Done');
        return true;
    }).catch(error => {
        console.log(error);
        return false;
    });
};

/**
 *
 * @param skipServerUpdate
 * @returns {Promise<void>}
 */
const deleteExistingWebHooks = async (skipServerUpdate = false) => {
    const webHooks = await bigCommerceWebhooks.getWebhooks();
    const promises = [];
    for (const id in webHooks) {
        const hook = webHooks[id];
        if (!skipServerUpdate) {
            promises.push(await deleteWebHook(hook.id));
        }
    }
    Promise.all(promises).then(() => {
        console.log('All done - for delete.');
    }).catch(error => {
        console.log(error);
    });
};

/**
 *
 * @param body
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const addWebHook = async (body, skipServerUpdate = false) => {
    let webHook = null;
    let retryCount = 0;
    let maxRetries = process.env.MAX_API_RETRIES;
    while (true) {
        try {
            if (!skipServerUpdate) {
                webHook = await bigCommerceWebhooks.addWebhook(body);
                restLog('WebHooks Result', webHook);
            }
            break;
        } catch (error) {
            if (++retryCount === maxRetries) {
                console.log(error);
                break;
            }
            console.log(error);
            await sleep(500);
        }
    }
    return webHook;
};

/***
 *
 * @param id
 * @param skipServerUpdate
 * @returns {Promise<null>}
 */
const deleteWebHook = async (id, skipServerUpdate = false) => {
    let webHook = null;
    let retryCount = 0;
    let maxRetries = process.env.MAX_API_RETRIES;
    while (true) {
        try {
            if (!skipServerUpdate) {
                webHook = await bigCommerceWebhooks.deleteWebhooks(id);
            }
            break;
        } catch (error) {
            if (++retryCount === maxRetries){
                break;
            }
            await sleep(500);
        }
    }
    return webHook;
};

/**
 * Process response for web hook
 * @param response
 */
const processResponse = (response) => {
    const { data, scope } = response;
    const { CUSTOMER, ORDER } = hookName;
    switch (data.type) {
        case CUSTOMER:
            hookCondition(data.id, scope);
            break;
        case ORDER:
            hookOrderCondition(data.id, scope);
            break;
        default:
            break;
    }
};

module.exports = {
  initialize,
  processResponse,
  deleteExistingWebHooks,
  deleteWebHook,
  addWebHook,
};
