const { hookScope } = require('./enums');
const { createOrder, updateOrder } = require('../service/order');

/***
 *
 * @param id
 * @param scope
 * @returns {Promise<void>}
 */
const hookOrderCondition = async (id, scope) => {
    const { CREATE_ORDER, UPDATE_ORDER } = hookScope;
    if (scope === CREATE_ORDER) {
        await createOrder(id);
    } else if (scope === UPDATE_ORDER) {
        await updateOrder(id);
    }
};

module.exports = {
    hookOrderCondition,
    createOrder,
};
